Preferences-Crypto
===============

This module contains implementation of `SharedPreferences` that supports both **keys** and **values**
**encryption** and **decryption**. All cryptography related operations are delegated by the crypto
preferences to a supplied **[Crypto](https://github.com/universum-studios/android_crypto)** 
implementations (for keys and values respectively).

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apreferences/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apreferences/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:preferences-crypto:${DESIRED_VERSION}@aar"

_depends on:_
[preferences-core](https://bitbucket.org/android-universum/preferences/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [CryptoSharedPreferences](https://bitbucket.org/android-universum/preferences/src/main/library-crypto/src/main/java/universum/studios/android/preference/crypto/CryptoSharedPreferences.java)