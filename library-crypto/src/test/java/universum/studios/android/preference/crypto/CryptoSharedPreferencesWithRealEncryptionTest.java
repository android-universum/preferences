/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.content.SharedPreferences;

import org.junit.Test;

import java.security.Key;
import java.util.HashSet;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import universum.studios.android.crypto.cipher.CipherBuilder;
import universum.studios.android.crypto.cipher.CipherCrypto;
import universum.studios.android.crypto.cipher.CipherCryptography;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CryptoSharedPreferencesWithRealEncryptionTest extends CryptoPreferencesTestCase {
    
	private static final String PREF_KEY = "PREFERENCE.Key";

	private final Key cipherKey;

	public CryptoSharedPreferencesWithRealEncryptionTest() throws Exception {
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		keyGenerator.init(128);
		this.cipherKey = keyGenerator.generateKey();
	}

	private static SharedPreferences buildCryptoPreferences(final SharedPreferences preferences, Key cipherKey) {
		return new CryptoSharedPreferences.Builder(preferences)
				.keyCrypto(new CipherCrypto.Builder()
						.encryptoCipher(createEncryptoCipher(cipherKey))
						.decryptoCipher(createDecryptoCipher(cipherKey))
						.build()
				)
				.valueCrypto(new CipherCrypto.Builder()
						.encryptoCipher(createEncryptoCipher(cipherKey))
						.decryptoCipher(createDecryptoCipher(cipherKey))
						.build()
				)
				.build();
	}

	private static Cipher createEncryptoCipher(Key cipherKey) {
		return new CipherBuilder()
				.transformation(CipherCryptography.createTransformation(
						CipherCryptography.Algorithm.AES,
						CipherCryptography.Mode.ECB,
						CipherCryptography.Padding.PKCS7
				))
				.opMode(Cipher.ENCRYPT_MODE)
				.key(cipherKey)
				.build();
	}

	private static Cipher createDecryptoCipher(Key cipherKey) {
		return new CipherBuilder()
				.transformation(CipherCryptography.createTransformation(
						CipherCryptography.Algorithm.AES,
						CipherCryptography.Mode.ECB,
						CipherCryptography.Padding.PKCS7
				))
				.opMode(Cipher.DECRYPT_MODE)
				.key(cipherKey)
				.build();
	}

	@Test public void testString() {
		// Arrange:
		final String[] values = {"Svarog", "Perún", "Choros", "Simargel", "Mokoš", "Živa", "Morena", "Vesna", "Veles"};
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Act + Assert:
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(editor.putString(PREF_KEY + "." + i, values[i]), is(editor));
		}
		assertThat(editor.commit(), is(true));
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(cryptoPreferences.getString(PREF_KEY + "." + i, "Default"), is(values[i]));
			assertThat(preferences.getString(PREF_KEY + "." + i, "Default"), is(not(values[i])));
		}
	}

	@Test public void testStringSet() {
		// Arrange:
		final Set<String> values = new HashSet<>(10);
		values.add("Svarog");
		values.add(Integer.toString(5));
		values.add(Long.toString(1000L));
		values.add(Float.toString(0.5f));
		values.add(Boolean.toString(false));
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		// Act + Assert:
		assertThat(cryptoPreferences.edit().putStringSet(PREF_KEY, values).commit(), is(true));
		assertThat(cryptoPreferences.getStringSet(PREF_KEY, null), is(values));
	}

	@Test public void testInt() {
		// Arrange:
		final int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1};
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Act + Assert:
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(editor.putInt(PREF_KEY + "." + i, values[i]), is(editor));
		}
		assertThat(editor.commit(), is(true));
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(cryptoPreferences.getInt(PREF_KEY + "." + i, 0), is(values[i]));
			assertThat(preferences.getInt(PREF_KEY + "." + i, 0), is(0));
			assertThat(preferences.getString(PREF_KEY + "." + i, "Default"), is(not(Integer.toString(values[i]))));
		}
	}

	@Test public void testLong() {
		// Arrange:
		final long[] values = {100L, 1000L, 10000L, 100000L, 1000000L, 10000000L};
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Act + Assert:
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(editor.putLong(PREF_KEY + "." + i, values[i]), is(editor));
		}
		assertThat(editor.commit(), is(true));
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(cryptoPreferences.getLong(PREF_KEY + "." + i, 0L), is(values[i]));
			assertThat(preferences.getLong(PREF_KEY + "." + i, 0L), is(0L));
			assertThat(preferences.getString(PREF_KEY + "." + i, "Default"), is(not(Long.toString(values[i]))));
		}
	}

	@Test public void testFloat() {
		// Arrange:
		final float[] values = {0.0f, 0.5f, -0.123456789f, -2.0f, 3.14f, 0.99999999f};
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Act + Assert:
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(editor.putFloat(PREF_KEY + "." + i, values[i]), is(editor));
		}
		assertThat(editor.commit(), is(true));
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(cryptoPreferences.getFloat(PREF_KEY + "." + i, -1.0f), is(values[i]));
			assertThat(preferences.getFloat(PREF_KEY + "." + i, -1.0f), is(-1.0f));
			assertThat(preferences.getString(PREF_KEY + "." + i, "Default"), is(not(Float.toString(values[i]))));
		}
	}

	@Test public void testBoolean() {
		// Arrange:
		final boolean[] values = {true, true, false, false, true, false, true, false};
		final SharedPreferences cryptoPreferences = buildCryptoPreferences(preferences, cipherKey);
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Act + Assert:
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(editor.putBoolean(PREF_KEY + "." + i, values[i]), is(editor));
		}
		assertThat(editor.commit(), is(true));
		for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
			assertThat(cryptoPreferences.getBoolean(PREF_KEY + "." + i, true), is(values[i]));
			assertThat(preferences.getBoolean(PREF_KEY + "." + i, false), is(false));
			assertThat(preferences.getString(PREF_KEY + "." + i, "Default"), is(not(Boolean.toString(values[i]))));
		}
	}
}