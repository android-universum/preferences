/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.content.SharedPreferences;

import org.junit.Test;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Albedinsky
 */
public final class CryptoSharedPreferencesChangeListenersRegistryTest extends TestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";

	@Test public void testListenerRegistration() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final CryptoSharedPreferences.ChangeListenersRegistry listeners = new CryptoSharedPreferences.ChangeListenersRegistry(
				new CryptoSharedPreferences.CryptoHelper(null, null),
				mockPreferences
		);
		// Act + Assert:
		listeners.register(mockListener);
		listeners.register(mockListener);
		listeners.unregister(mockListener);
		assertThat(listeners.isEmpty(), is(true));
		listeners.register(mockListener);
		listeners.register(mock(SharedPreferences.OnSharedPreferenceChangeListener.class));
		listeners.unregister(mockListener);
		listeners.unregister(mockListener);
		assertThat(listeners.isEmpty(), is(false));
	}

	@Test public void testIsEmptyWithoutRegisteredAnyListeners() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final CryptoSharedPreferences.ChangeListenersRegistry listeners = new CryptoSharedPreferences.ChangeListenersRegistry(
				new CryptoSharedPreferences.CryptoHelper(null, null),
				mockPreferences
		);
		// Act + Assert:
		assertThat(listeners.isEmpty(), is(true));
	}

	@Test public void testOnSharedPreferenceChangedWithRegisteredListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final CryptoSharedPreferences.ChangeListenersRegistry listeners = new CryptoSharedPreferences.ChangeListenersRegistry(
				new CryptoSharedPreferences.CryptoHelper(null, null),
				mockPreferences
		);
		listeners.register(mockListener);
		final SharedPreferences changedPreferences = mock(SharedPreferences.class);
		// Act:
		listeners.onSharedPreferenceChanged(changedPreferences, PREF_KEY + ".First");
		listeners.onSharedPreferenceChanged(changedPreferences, PREF_KEY + ".Second");
		// Assert:
		verify(mockListener).onSharedPreferenceChanged(mockPreferences, PREF_KEY + ".First");
		verify(mockListener).onSharedPreferenceChanged(mockPreferences, PREF_KEY + ".Second");
		verify(mockListener, times(0)).onSharedPreferenceChanged(changedPreferences, PREF_KEY);
	}

	@Test public void testOnSharedPreferenceChangedWithUnregisteredListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final CryptoSharedPreferences.ChangeListenersRegistry listeners = new CryptoSharedPreferences.ChangeListenersRegistry(
				new CryptoSharedPreferences.CryptoHelper(null, null),
				mockPreferences
		);
		listeners.register(mockListener);
		final SharedPreferences changedPreferences = mock(SharedPreferences.class);
		listeners.unregister(mockListener);
		listeners.onSharedPreferenceChanged(changedPreferences, PREF_KEY);
		verify(mockListener, times(0)).onSharedPreferenceChanged(mockPreferences, PREF_KEY);
	}

	@Test public void testOnSharedPreferenceChangedWithoutRegisteredListeners() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final CryptoSharedPreferences.ChangeListenersRegistry listeners = new CryptoSharedPreferences.ChangeListenersRegistry(
				new CryptoSharedPreferences.CryptoHelper(null, null),
				mockPreferences
		);
		// Act:
		listeners.onSharedPreferenceChanged(mock(SharedPreferences.class), PREF_KEY);
	}
}