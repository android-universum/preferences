/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.content.SharedPreferences;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import universum.studios.android.preference.SharedPreferencesCache;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class CryptoSharedPreferencesEditorTest extends CryptoPreferencesTestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";

	@Test public void testCache() {
		// Arrange:
		final SharedPreferencesCache mockCache = mock(SharedPreferencesCache.class);
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		// Act:
		editor.setCache(mockCache);
		editor.putString(PREF_KEY, "pref.value");
		// Assert:
		verify(mockCache).evict(PREF_KEY);
	}

	@Test public void testString() {
		// Arrange:
		final String prefValue = "pref.value";
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		when(mockHelper.encryptValue(prefValue)).thenReturn(prefValue);
		// Act + Assert:
		assertThat(editor.putString(PREF_KEY, prefValue), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putString(PREF_KEY, prefValue);
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValue(prefValue);
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testStringSet() {
		// Arrange:
		final Set<String> prefValues = new HashSet<>();
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		// Act + Assert:
		assertThat(editor.putStringSet(PREF_KEY, prefValues), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putStringSet(PREF_KEY, prefValues);
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValuesSet(prefValues);
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testInt() {
		// Arrange:
		final int prefValue = 1;
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		when(mockHelper.encryptValue(Integer.toString(prefValue))).thenReturn(Integer.toString(prefValue));
		// Act + Assert:
		assertThat(editor.putInt(PREF_KEY, prefValue), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putString(PREF_KEY, Integer.toString(prefValue));
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValue(Integer.toString(prefValue));
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testFloat() {
		// Arrange:
		final float prefValue = 0.5f;
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		when(mockHelper.encryptValue(Float.toString(prefValue))).thenReturn(Float.toString(prefValue));
		// Act + Assert:
		assertThat(editor.putFloat(PREF_KEY, prefValue), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putString(PREF_KEY, Float.toString(prefValue));
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValue(Float.toString(prefValue));
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testLong() {
		// Arrange:
		final long prefValue = 1000L;
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		when(mockHelper.encryptValue(Long.toString(prefValue))).thenReturn(Long.toString(prefValue));
		// Act + Assert:
		assertThat(editor.putLong(PREF_KEY, prefValue), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putString(PREF_KEY, Long.toString(prefValue));
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValue(Long.toString(prefValue));
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testBoolean() {
		// Arrange:
		final boolean prefValue = true;
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		when(mockHelper.encryptKey(PREF_KEY)).thenReturn(PREF_KEY);
		when(mockHelper.encryptValue(Boolean.toString(prefValue))).thenReturn(Boolean.toString(prefValue));
		// Act + Assert:
		assertThat(editor.putBoolean(PREF_KEY, prefValue), is((SharedPreferences.Editor) editor));
		verify(mockDelegate).putString(PREF_KEY, Boolean.toString(prefValue));
		verify(mockHelper).encryptKey(PREF_KEY);
		verify(mockHelper).encryptValue(Boolean.toString(prefValue));
		verifyNoMoreInteractions(mockDelegate, mockHelper);
	}

	@Test public void testClear() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		// Act:
		editor.clear();
		// Assert:
		verify(mockDelegate).clear();
	}

	@Test public void testRemove() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		// Act:
		editor.remove(PREF_KEY);
		// Assert:
		verify(mockDelegate).remove(PREF_KEY);
	}

	@Test public void testCommit() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		// Act:
		editor.commit();
		// Assert:
		verify(mockDelegate).commit();
		verifyNoMoreInteractions(mockDelegate);
	}

	@Test public void testApply() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper mockHelper = mock(CryptoSharedPreferences.CryptoHelper.class);
		final SharedPreferences.Editor mockDelegate = mock(SharedPreferences.Editor.class);
		final CryptoSharedPreferences.CryptoEditor editor = new CryptoSharedPreferences.CryptoEditor(mockHelper, mockDelegate);
		// Act:
		editor.apply();
		// Assert:
		verify(mockDelegate).apply();
		verifyNoMoreInteractions(mockDelegate);
	}
}