/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.content.SharedPreferences;

import universum.studios.android.preference.SharedPreferencesPolicies;
import universum.studios.android.preference.SimpleSharedPreferencesFacade;
import universum.studios.android.test.AndroidTestCase;

/**
 * @author Martin Albedinsky
 */
abstract class CryptoPreferencesTestCase extends AndroidTestCase {

	SharedPreferences preferences;

	@Override public void beforeTest() {
		super.beforeTest();
		this.preferences = context().getSharedPreferences(
				context().getPackageName() + ":test_crypto_preferences",
				SharedPreferencesPolicies.MODE_PRIVATE
		);
		// Ensure that we have a clean slate before each test.
		new SimpleSharedPreferencesFacade(preferences).removeAll();
	}

	@Override public void afterTest() {
		super.afterTest();
		this.preferences = null;
	}
}