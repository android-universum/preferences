/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.util.Base64;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.Cryptography;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class CryptoSharedPreferencesHelperTest extends CryptoPreferencesTestCase {

	private static final String VALUE = "Value";
	private static final String KEY = "PREFERENCE.Key";

	@Test public void testAreValuesEqual() {
		// Act + Assert:
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(null, null), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(null, ""), is(false));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual("", null), is(false));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual("s", "s"), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(1, 1), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(1f, 1f), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(1L, 1L), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(1D, 1D), is(true));
		assertThat(CryptoSharedPreferences.CryptoHelper.areValuesEqual(false, false), is(true));
	}

	@Test public void testEncryptKey() throws Exception {
		// Arrange:
		final byte[] rawKey = bytesOf(KEY);
		final String encodedKey = encode(KEY);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.encrypt(rawKey)).thenReturn(rawKey);
		// Act + Assert:
		assertThat(helper.encryptKey(KEY), is(encodedKey));
		verify(mockCrypto).encrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testEncryptKeyOnEmptyHelper() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		// Act + Assert:
		assertThat(helper.encryptKey(KEY), is(KEY));
	}

	@Test public void testDecryptKey() throws Exception {
		// Arrange:
		final byte[] rawKey = bytesOf(KEY);
		final String encodedKey = encode(KEY);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.decrypt(rawKey)).thenReturn(rawKey);
		// Act + Assert:
		assertThat(helper.decryptKey(encodedKey), is(KEY));
		verify(mockCrypto).decrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testDecryptKeyOnEmptyHelper() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		// Act + Assert:
		assertThat(helper.decryptKey(KEY), is(KEY));
	}

	@Test public void testEncryptValue() throws Exception {
		// Arrange:
		final byte[] rawValue = bytesOf(VALUE);
		final String encodedValue = encode(VALUE);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.encrypt(rawValue)).thenReturn(rawValue);
		// Act + Assert:
		assertThat(helper.encryptValue(VALUE), is(encodedValue));
		verify(mockCrypto).encrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testEncryptValueWithEmptyHelper() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		// Act + Assert:
		assertThat(helper.encryptValue(VALUE), is(VALUE));
	}

	@Test public void testDecryptValue() throws Exception {
		// Arrange:
		final byte[] rawValue = bytesOf(VALUE);
		final String encodedValue = encode(VALUE);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.decrypt(rawValue)).thenReturn(rawValue);
		// Act + Assert:
		assertThat(helper.decryptValue(encodedValue), is(VALUE));
		verify(mockCrypto).decrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testDecryptValueWithEmptyHelper() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		// Act + Assert:
		assertThat(helper.decryptValue(VALUE), is(VALUE));
	}

	@Test public void testEncryptValuesSet() throws Exception {
		// Arrange:
		final byte[] rawValue = bytesOf(VALUE);
		final String encodedValue = encode(VALUE);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.encrypt(rawValue)).thenReturn(rawValue);
		final Set<String> valuesSet = new HashSet<>(2);
		valuesSet.addAll(Arrays.asList(VALUE, VALUE));
		final Set<String> encodedValuesSet = new HashSet<>(2);
		encodedValuesSet.addAll(Arrays.asList(encodedValue, encodedValue));
		// Act + Assert:
		assertThat(helper.encryptValuesSet(valuesSet), is(encodedValuesSet));
		verify(mockCrypto, times(encodedValuesSet.size())).encrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testEncryptNullValuesSet() {
		// Arrange:
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		// Act + Assert:
		assertThat(helper.encryptValuesSet(null), is(nullValue()));
		verifyZeroInteractions(mockCrypto);
	}

	@Test public void testEncryptValuesSetWithEmptyHelper() {
		// Arrange:
		final Set<String> valuesSet = new HashSet<>(0);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		// Act + Assert:
		assertThat(helper.encryptValuesSet(valuesSet), is(valuesSet));
	}

	@Test public void testDecryptValuesSet() throws Exception {
		// Arrange:
		final byte[] rawValue = bytesOf(VALUE);
		final String encodedValue = encode(VALUE);
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		when(mockCrypto.decrypt(rawValue)).thenReturn(rawValue);
		final Set<String> valuesSet = new HashSet<>(2);
		valuesSet.addAll(Arrays.asList(VALUE, VALUE));
		final Set<String> encodedValuesSet = new HashSet<>(2);
		encodedValuesSet.addAll(Arrays.asList(encodedValue, encodedValue));
		// Act + Assert:
		assertThat(helper.decryptValuesSet(encodedValuesSet), is(valuesSet));
		verify(mockCrypto, times(valuesSet.size())).decrypt(any(byte[].class));
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testDecryptNullValuesSet() {
		// Arrange:
		final Crypto mockCrypto = mock(Crypto.class);
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(mockCrypto, mockCrypto);
		// Act + Assert:
		assertThat(helper.decryptValuesSet(null), is(nullValue()));
		verifyZeroInteractions(mockCrypto);
	}

	@Test public void testDecryptValuesSetWithEmptyHelper() {
		// Arrange:
		final CryptoSharedPreferences.CryptoHelper helper = new CryptoSharedPreferences.CryptoHelper(null, null);
		final Set<String> valuesSet = new HashSet<>(0);
		// Act + Assert:
		assertThat(helper.decryptValuesSet(valuesSet), is(valuesSet));
	}

	private static byte[] bytesOf(final String value) throws UnsupportedEncodingException {
		return value.getBytes(Cryptography.CHARSET_NAME);
	}

	private static String encode(final String value) throws Exception {
		return Base64.encodeToString(value.getBytes(Cryptography.CHARSET_NAME), Base64.NO_WRAP);
	}
}