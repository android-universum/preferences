/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.util.Base64;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.crypto.Cryptography;
import universum.studios.android.crypto.Decrypto;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("deprecation")
public final class CryptoUtilsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		CryptoUtils.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CryptoUtils> constructor = CryptoUtils.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testEncrypt() throws Exception {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		when(mockEncrypto.encrypt("data".getBytes(Cryptography.CHARSET_NAME))).thenReturn("data".getBytes(Cryptography.CHARSET_NAME));
		// Act:
		final String encrypted = CryptoUtils.encrypt("data", mockEncrypto);
		// Assert:
		assertThat(encrypted, is(Base64.encodeToString("data".getBytes(Cryptography.CHARSET_NAME), Base64.NO_WRAP)));
		verify(mockEncrypto).encrypt("data".getBytes(Cryptography.CHARSET_NAME));
	}

	@Test public void testEncryptEmptyValue() {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		// Act:
		final String encrypted = CryptoUtils.encrypt("", mockEncrypto);
		// Assert:
		assertThat(encrypted, is(""));
		verify(mockEncrypto, times(0)).encrypt(any(byte[].class));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testEncryptNullValue() {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		// Act:
		final String encrypted = CryptoUtils.encrypt(null, mockEncrypto);
		// Assert:
		assertThat(encrypted, is(nullValue()));
		verify(mockEncrypto, times(0)).encrypt(any(byte[].class));
	}

	@Test public void testDecrypt() throws Exception {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		final byte[] decodedDataBytes = Base64.decode("data", Base64.DEFAULT);
		when(mockDecrypto.decrypt(decodedDataBytes)).thenReturn("data".getBytes(Cryptography.CHARSET_NAME));
		// Act:
		final String decrypted = CryptoUtils.decrypt("data", mockDecrypto);
		// Assert:
		assertThat(decrypted, is("data"));
		verify(mockDecrypto).decrypt(decodedDataBytes);
	}

	@Test public void testDecryptEmptyValue() {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		// Act:
		final String decrypted = CryptoUtils.decrypt("", mockDecrypto);
		// Assert:
		assertThat(decrypted, is(""));
		verify(mockDecrypto, times(0)).decrypt(any(byte[].class));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testDecryptNullValue() {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		// Act:
		final String decrypted = CryptoUtils.decrypt(null, mockDecrypto);
		// Assert:
		assertThat(decrypted, is(nullValue()));
		verify(mockDecrypto, times(0)).decrypt(any(byte[].class));
	}
}