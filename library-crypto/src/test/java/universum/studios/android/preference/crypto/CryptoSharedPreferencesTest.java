/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.content.SharedPreferences;
import android.os.Build;

import org.junit.Test;
import org.mockito.Mockito;
import org.robolectric.annotation.Config;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.Cryptography;
import universum.studios.android.preference.SharedPreferencesCache;
import universum.studios.android.preference.cache.SharedPreferenceCaches;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class CryptoSharedPreferencesTest extends CryptoPreferencesTestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";
	private static final Crypto CRYPTO = TestCryptos.create();

	private Crypto mockKeyCrypto, mockValueCrypto;
	private SharedPreferencesCache mockCache;
	private SharedPreferencesCache cache;
	private CryptoSharedPreferences cryptoPreferences;
	private byte[] rawDecryptedKey, rawEncryptedKey;

	@Override public void beforeTest() {
		super.beforeTest();
		this.mockKeyCrypto = mock(Crypto.class);
		this.rawDecryptedKey = bytesOf(PREF_KEY);
		this.rawEncryptedKey = CRYPTO.encrypt(rawDecryptedKey);
		when(mockKeyCrypto.encrypt(rawDecryptedKey)).thenReturn(rawEncryptedKey);
		when(mockKeyCrypto.decrypt(rawEncryptedKey)).thenReturn(rawDecryptedKey);
		this.mockValueCrypto = mock(Crypto.class);
		this.mockCache = mock(SharedPreferencesCache.class);
		this.cache = SharedPreferenceCaches.mapCache();
		this.cryptoPreferences = new CryptoSharedPreferences.Builder(preferences)
				.keyCrypto(mockKeyCrypto)
				.valueCrypto(mockValueCrypto)
				.valueCache(cache)
				.build();
	}

	@Override public void afterTest() {
		super.afterTest();
		this.cache = null;
		this.mockKeyCrypto = null;
		this.mockValueCrypto = null;
		this.mockCache = null;
		this.cryptoPreferences = null;
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBuildWithoutValueCrypto() {
		// Act:
		new CryptoSharedPreferences.Builder(preferences).build();
	}

	@Test public void testRegisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final byte[] rawValue = bytesOf("Value");
		when(mockValueCrypto.encrypt(rawValue)).thenReturn(CRYPTO.encrypt(rawValue));
		final SharedPreferences.OnSharedPreferenceChangeListener firstMockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences.OnSharedPreferenceChangeListener secondMockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		// Act:
		cryptoPreferences.registerOnSharedPreferenceChangeListener(firstMockListener);
		cryptoPreferences.registerOnSharedPreferenceChangeListener(firstMockListener);
		cryptoPreferences.registerOnSharedPreferenceChangeListener(secondMockListener);
		// Assert:
		assertThat(cryptoPreferences.edit().putString(PREF_KEY, "Value").commit(), is(true));
		verify(mockKeyCrypto).decrypt(rawEncryptedKey);
		verify(firstMockListener).onSharedPreferenceChanged(cryptoPreferences, PREF_KEY);
		verify(secondMockListener).onSharedPreferenceChanged(cryptoPreferences, PREF_KEY);
		cryptoPreferences.unregisterOnSharedPreferenceChangeListener(firstMockListener);
		cryptoPreferences.unregisterOnSharedPreferenceChangeListener(secondMockListener);
		verifyNoMoreInteractions(firstMockListener, secondMockListener);
	}

	@Test public void testUnregisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final byte[] rawValue = bytesOf("Value");
		when(mockValueCrypto.encrypt(rawValue)).thenReturn(CRYPTO.encrypt(rawValue));
		final SharedPreferences.OnSharedPreferenceChangeListener firstMockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences.OnSharedPreferenceChangeListener secondMockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		cryptoPreferences.registerOnSharedPreferenceChangeListener(firstMockListener);
		cryptoPreferences.registerOnSharedPreferenceChangeListener(secondMockListener);
		// Act:
		cryptoPreferences.unregisterOnSharedPreferenceChangeListener(firstMockListener);
		cryptoPreferences.unregisterOnSharedPreferenceChangeListener(secondMockListener);
		// Assert:
		assertThat(cryptoPreferences.edit().putString(PREF_KEY, "Value").commit(), is(true));
		verify(mockKeyCrypto, times(0)).decrypt(bytesOf(PREF_KEY));
		verifyZeroInteractions(firstMockListener, secondMockListener);
	}

	@Test public void testGetAll() {
		// Arrange:
		final CryptoSharedPreferences cryptoPreferences = new CryptoSharedPreferences.Builder(preferences).valueCrypto(CRYPTO).build();
		final CryptoSharedPreferences.Editor editor = cryptoPreferences.edit();
		editor.putString(PREF_KEY + ".Null", null);
		editor.putString(PREF_KEY + ".String", "Value");
		editor.putInt(PREF_KEY + ".Integer", 10);
		editor.putFloat(PREF_KEY + ".Float", 0.5f);
		editor.putLong(PREF_KEY + ".Long", 1000L);
		editor.putBoolean(PREF_KEY + ".Boolean", true);
		editor.commit();
		// Act:
		final Map<String, ?> values = cryptoPreferences.getAll();
		// Assert:
		assertThat(values, is(notNullValue()));
		assertThat(values.size(), is(5));
		assertThat(values.get(PREF_KEY + ".Null"), is(nullValue()));
		assertThat(values.get(PREF_KEY + ".String"), is("Value"));
		assertThat(values.get(PREF_KEY + ".Integer"), is(Integer.toString(10)));
		assertThat(values.get(PREF_KEY + ".Float"), is(Float.toString(0.5f)));
		assertThat(values.get(PREF_KEY + ".Long"), is(Long.toString(1000L)));
		assertThat(values.get(PREF_KEY + ".Boolean"), is(Boolean.toString(true)));
	}

	@SuppressWarnings("unchecked")
	@Test public void testGetAllWithStringSet() {
		// Arrange:
		final CryptoSharedPreferences cryptoPreferences = new CryptoSharedPreferences.Builder(preferences).valueCrypto(CRYPTO).build();
		final Set<String> valuesSetIn = new HashSet<>(2);
		valuesSetIn.addAll(Arrays.asList("Value1", "Value2"));
		cryptoPreferences.edit().putStringSet(PREF_KEY, valuesSetIn).commit();
		// Act:
		final Map<String, ?> values = cryptoPreferences.getAll();
		// Assert:
		assertThat(values, is(notNullValue()));
		assertThat(values.size(), is(1));
		final Set<String> valuesSetOut = (Set<String>) values.get(PREF_KEY);
		assertThat(valuesSetOut, is(valuesSetIn));
	}

	@Test(expected = IllegalStateException.class)
	public void testGetAllWithUnsupportedValue() {
		// Arrange:
		preferences.edit().putInt(PREF_KEY, 1).commit();
		// Act:
		new CryptoSharedPreferences.Builder(preferences).valueCrypto(CRYPTO).build().getAll();
	}

	@Test public void testGetAllForEmptyPreferences() {
		// Act:
		final Map<String, ?> values = cryptoPreferences.getAll();
		// Assert:
		assertThat(values, is(notNullValue()));
		assertThat(values.size(), is(0));
	}

	@Test public void testContains() {
		// Arrange:
		final String value = "Value";
		final byte[] rawValue = bytesOf(value);
		when(mockValueCrypto.encrypt(rawValue)).thenReturn(CRYPTO.encrypt(rawValue));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		// Act + Assert:
		assertThat(cryptoPreferences.contains(PREF_KEY), is(false));
		assertThat(cryptoPreferences.edit().putString(PREF_KEY, value).commit(), is(true));
		assertThat(cryptoPreferences.contains(PREF_KEY), is(true));
		verify(mockKeyCrypto, times(3)).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).encrypt(rawValue);
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetStringWithPersistedValue() {
		// Arrange:
		final String value = "Value";
		final byte[] rawValue = bytesOf(value);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		cryptoPreferences.edit().putString(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getString(PREF_KEY, "DefaultValue"), is(value));
		assertThat(cryptoPreferences.getString(PREF_KEY, "DefaultValue"), is(value));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getString(PREF_KEY), is(value));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).decrypt(CRYPTO.encrypt(rawValue));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetStringWithoutPersistedValue() {
		// Act + Assert:
		assertThat(cryptoPreferences.getString(PREF_KEY, "DefaultValue"), is("DefaultValue"));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetStringWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final String value = "Value";
		final byte[] rawValue = bytesOf(value);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		when(mockCache.putString(PREF_KEY, value)).thenReturn(true);
		preferences.edit().putString(PREF_KEY, value).commit();
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		Mockito.clearInvocations(mockCache);
		// Act + Assert:
		assertThat(preferences.getString(PREF_KEY, "DefaultValue"), is(value));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getString(PREF_KEY)).thenReturn(value);
		assertThat(preferences.getString(PREF_KEY, "DefaultValue"), is(value));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putString(PREF_KEY, value);
		verify(mockCache).getString(PREF_KEY);
		verifyNoMoreInteractions(mockCache);
	}

	@Test public void testGetStringWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final String value = "Value";
		final byte[] rawValue = bytesOf(value);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putString(PREF_KEY, value).commit();
		// Act + Assert:
		assertThat(preferences.getString(PREF_KEY, "DefaultValue"), is(value));
		assertThat(preferences.getString(PREF_KEY, "DefaultValue"), is(value));
	}

	@Test public void testGetStringSetWithPersistedValue() {
		// Arrange:
		final byte[] rawValue1 = bytesOf("Value1");
		final byte[] rawValue2 = bytesOf("Value2");
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue1);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue2);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue1));
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue2));
		final Set<String> values = new HashSet<>(2);
		values.addAll(Arrays.asList("Value1", "Value2"));
		cryptoPreferences.edit().putStringSet(PREF_KEY, values).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getStringSet(PREF_KEY, null), is(values));
		assertThat(cryptoPreferences.getStringSet(PREF_KEY, null), is(values));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getStringSet(PREF_KEY), is(values));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto, times(2)).decrypt(any(byte[].class));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testGetStringSetWithoutPersistedValue() {
		// Arrange:
		final Set<String> values = new HashSet<>(0);
		// Act + Assert:
		assertThat(cryptoPreferences.getStringSet(PREF_KEY, values), is(values));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto);
		verifyZeroInteractions(mockValueCrypto);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testGetStringSetWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final byte[] rawValue1 = bytesOf("Value1");
		final byte[] rawValue2 = bytesOf("Value2");
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue1);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue2);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue1));
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue2));
		final Set<String> values = new HashSet<>(2);
		values.addAll(Arrays.asList("Value1", "Value2"));
		when(mockCache.putStringSet(PREF_KEY, values)).thenReturn(true);
		preferences.edit().putStringSet(PREF_KEY, values).commit();
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		// Act + Assert:
		assertThat(preferences.getStringSet(PREF_KEY, null), is(values));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getStringSet(PREF_KEY)).thenReturn(values);
		assertThat(preferences.getStringSet(PREF_KEY, null), is(values));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putStringSet(PREF_KEY, values);
		verify(mockCache).getStringSet(PREF_KEY);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testGetStringSetWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final byte[] rawValue1 = bytesOf("Value1");
		final byte[] rawValue2 = bytesOf("Value2");
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue1);
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue2);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue1));
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue2));
		final Set<String> values = new HashSet<>(2);
		values.addAll(Arrays.asList("Value1", "Value2"));
		preferences.edit().putStringSet(PREF_KEY, values).commit();
		// Act + Assert:
		assertThat(preferences.getStringSet(PREF_KEY, null), is(values));
		assertThat(preferences.getStringSet(PREF_KEY, null), is(values));
	}

	@Test public void testGetIntWithPersistedValue() {
		// Arrange:
		final int value = 500;
		final byte[] rawValue = bytesOf(Integer.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		cryptoPreferences.edit().putInt(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getInt(PREF_KEY, -1), is(value));
		assertThat(cryptoPreferences.getInt(PREF_KEY, -1), is(value));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getInt(PREF_KEY), is(value));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).decrypt(CRYPTO.encrypt(rawValue));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetIntWithoutPersistedValue() {
		// Act + Assert:
		assertThat(cryptoPreferences.getInt(PREF_KEY, -1), is(-1));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto);
		verifyZeroInteractions(mockValueCrypto);
	}

	@Test public void testGetIntWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final int value = 500;
		final byte[] rawValue = bytesOf(Integer.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putInt(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockCache);
		// Act + Assert:
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		assertThat(preferences.getInt(PREF_KEY, -1), is(value));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getInt(PREF_KEY)).thenReturn(value);
		assertThat(preferences.getInt(PREF_KEY, -1), is(value));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putInt(PREF_KEY, value);
		verify(mockCache).getInt(PREF_KEY);
		verifyNoMoreInteractions(mockCache);
	}

	@Test public void testGetIntWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final int value = 500;
		final byte[] rawValue = bytesOf(Integer.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putInt(PREF_KEY, value).commit();
		// Act + Assert:
		assertThat(preferences.getInt(PREF_KEY, -1), is(value));
		assertThat(preferences.getInt(PREF_KEY, -1), is(value));
	}

	@Test public void testGetFloatWithPersistedValue() {
		// Arrange:
		final float value = 0.5f;
		final byte[] rawValue = bytesOf(Float.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		cryptoPreferences.edit().putFloat(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getFloat(PREF_KEY, -0.5f), is(value));
		assertThat(cryptoPreferences.getFloat(PREF_KEY, -0.5f), is(value));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getFloat(PREF_KEY), is(value));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).decrypt(CRYPTO.encrypt(rawValue));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetFloatWithoutPersistedValue() {
		// Act + Assert:
		assertThat(cryptoPreferences.getFloat(PREF_KEY, -0.5f), is(-0.5f));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto);
		verifyZeroInteractions(mockValueCrypto);
	}

	@Test public void testGetFloatWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final float value = 500;
		final byte[] rawValue = bytesOf(Float.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putFloat(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockCache);
		// Act + Assert:
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		assertThat(preferences.getFloat(PREF_KEY, -0.5f), is(value));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getFloat(PREF_KEY)).thenReturn(value);
		assertThat(preferences.getFloat(PREF_KEY, -0.5f), is(value));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putFloat(PREF_KEY, value);
		verify(mockCache).getFloat(PREF_KEY);
		verifyNoMoreInteractions(mockCache);
	}

	@Test public void testGetFloatWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final float value = 0.5f;
		final byte[] rawValue = bytesOf(Float.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putFloat(PREF_KEY, value).commit();
		// Act + Assert:
		assertThat(preferences.getFloat(PREF_KEY, -0.5f), is(value));
		assertThat(preferences.getFloat(PREF_KEY, -0.5f), is(value));
	}
	
	@Test public void testGetLongWithPersistedValue() {
		// Arrange:
		final long value = 1245L;
		final byte[] rawValue = bytesOf(Long.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		cryptoPreferences.edit().putLong(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getLong(PREF_KEY, 0L), is(value));
		assertThat(cryptoPreferences.getLong(PREF_KEY, 0L), is(value));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getLong(PREF_KEY), is(value));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).decrypt(CRYPTO.encrypt(rawValue));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetLongWithoutPersistedValue() {
		// Act + Assert:
		assertThat(cryptoPreferences.getLong(PREF_KEY, 0L), is(0L));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto);
		verifyZeroInteractions(mockValueCrypto);
	}

	@Test public void testGetLongWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final long value = 1245L;
		final byte[] rawValue = bytesOf(Long.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putLong(PREF_KEY, value).commit();
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		Mockito.clearInvocations(mockCache);
		// Act + Assert:
		assertThat(preferences.getLong(PREF_KEY, 0L), is(value));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getLong(PREF_KEY)).thenReturn(value);
		assertThat(preferences.getLong(PREF_KEY, 0L), is(value));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putLong(PREF_KEY, value);
		verify(mockCache).getLong(PREF_KEY);
		verifyNoMoreInteractions(mockCache);
	}

	@Test public void testGetLongWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final long value = 1245L;
		final byte[] rawValue = bytesOf(Long.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putLong(PREF_KEY, value).commit();
		// Act + Assert:
		assertThat(preferences.getLong(PREF_KEY, 0L), is(value));
		assertThat(preferences.getLong(PREF_KEY, 0L), is(value));
	}

	@Test public void testGetBooleanWithPersistedValue() {
		// Arrange:
		final boolean value = true;
		final byte[] rawValue = bytesOf(Boolean.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		cryptoPreferences.edit().putBoolean(PREF_KEY, value).commit();
		Mockito.clearInvocations(mockKeyCrypto, mockValueCrypto);
		// Act + Assert:
		assertThat(cryptoPreferences.getBoolean(PREF_KEY, false), is(value));
		assertThat(cryptoPreferences.getBoolean(PREF_KEY, false), is(value));
		assertThat(cache.contains(PREF_KEY), is(true));
		assertThat(cache.getBoolean(PREF_KEY), is(value));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verify(mockValueCrypto).decrypt(CRYPTO.encrypt(rawValue));
		verifyNoMoreInteractions(mockKeyCrypto, mockValueCrypto);
	}

	@Test public void testGetBooleanWithoutPersistedValue() {
		// Act + Assert:
		assertThat(cryptoPreferences.getBoolean(PREF_KEY, true), is(true));
		assertThat(cache.contains(PREF_KEY), is(false));
		verify(mockKeyCrypto).encrypt(rawDecryptedKey);
		verifyNoMoreInteractions(mockKeyCrypto);
		verifyZeroInteractions(mockValueCrypto);
	}

	@Test public void testGetBooleanWithCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithCache();
		final boolean value = true;
		final byte[] rawValue = bytesOf(Boolean.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putBoolean(PREF_KEY, value).commit();
		when(mockCache.contains(PREF_KEY)).thenReturn(false);
		Mockito.clearInvocations(mockCache);
		// Act + Assert:
		assertThat(preferences.getBoolean(PREF_KEY, false), is(value));
		when(mockCache.contains(PREF_KEY)).thenReturn(true);
		when(mockCache.getBoolean(PREF_KEY)).thenReturn(value);
		assertThat(preferences.getBoolean(PREF_KEY, false), is(value));
		verify(mockCache, times(2)).contains(PREF_KEY);
		verify(mockCache).putBoolean(PREF_KEY, value);
		verify(mockCache).getBoolean(PREF_KEY);
		verifyNoMoreInteractions(mockCache);
	}

	@Test public void testGetBooleanWithoutCache() {
		// Arrange:
		final CryptoSharedPreferences preferences = buildCryptoPreferencesWithoutCache();
		final boolean value = true;
		final byte[] rawValue = bytesOf(Boolean.toString(value));
		whenCryptoEncryptThanReturn(mockValueCrypto, rawValue);
		whenCryptoDecryptThanReturn(mockValueCrypto, CRYPTO.encrypt(rawValue));
		preferences.edit().putBoolean(PREF_KEY, value).commit();
		// Act + Assert:
		assertThat(preferences.getBoolean(PREF_KEY, false), is(value));
		assertThat(preferences.getBoolean(PREF_KEY, false), is(value));
	}

	@Test public void testEdit() {
		// Act:
		final SharedPreferences.Editor editor = cryptoPreferences.edit();
		// Assert:
		assertThat(editor, is(notNullValue()));
		assertThat(editor, is(cryptoPreferences.edit()));
	}

	private static byte[] bytesOf(String value) {
		try {
			return value.getBytes(Cryptography.CHARSET_NAME);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		throw new IllegalStateException();
	}

	private CryptoSharedPreferences buildCryptoPreferencesWithCache() {
		return new CryptoSharedPreferences.Builder(preferences).valueCrypto(mockValueCrypto).valueCache(mockCache).build();
	}

	private CryptoSharedPreferences buildCryptoPreferencesWithoutCache() {
		return new CryptoSharedPreferences.Builder(preferences).valueCrypto(mockValueCrypto).build();
	}

	private static void whenCryptoEncryptThanReturn(Crypto crypto, byte[] decryptedData) {
		when(crypto.encrypt(decryptedData)).thenReturn(CRYPTO.encrypt(decryptedData));
	}

	private static void whenCryptoDecryptThanReturn(Crypto crypto, byte[] encryptedData) {
		when(crypto.decrypt(encryptedData)).thenReturn(CRYPTO.decrypt(encryptedData));
	}
}