/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.crypto;

import android.util.Base64;

import androidx.annotation.NonNull;
import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.CryptographyException;

/**
 * @author Martin Albedinsky
 */
final class TestCryptos {

	private TestCryptos() {}

	@NonNull static Crypto create() {
		return new Crypto() {

			@Override @NonNull public byte[] encrypt(@NonNull final byte[] data) throws CryptographyException {
				return Base64.encode(data, Base64.DEFAULT);
			}

			@Override @NonNull public byte[] decrypt(@NonNull final byte[] data) throws CryptographyException {
				return Base64.decode(data, Base64.DEFAULT);
			}
		};
	}
}