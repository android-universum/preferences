Preferences-Common
===============

This module contains **common** implementations of `SharedPreference` which may be used to simplify
accessing of common values persisted in `SharedPreferences`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apreferences/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apreferences/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:preferences-common:${DESIRED_VERSION}@aar"

_depends on:_
[preferences-core](https://bitbucket.org/android-universum/preferences/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [StringPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/StringPreference.java)
- [IntegerPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/IntegerPreference.java)
- [FloatPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/FloatPreference.java)
- [LongPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/LongPreference.java)
- [BooleanPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/BooleanPreference.java)
- [EnumPreference](https://bitbucket.org/android-universum/preferences/src/main/library-common/src/main/java/universum/studios/android/preference/EnumPreference.java)