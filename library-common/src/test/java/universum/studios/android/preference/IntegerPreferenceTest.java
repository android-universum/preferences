/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class IntegerPreferenceTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Integer";

	@Test public void testInstantiation() {
		// Arrange:
		final IntegerPreference preference = new IntegerPreference(PREF_KEY, 3);
		// Act + Assert:
		assertThat(preference.getKey(), is(PREF_KEY));
		assertThat(preference.getValue(), is(3));
		assertThat(preference.getDefaultValue(), is(3));
	}

	@Test public void testPersistence() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:integer", Context.MODE_PRIVATE);
		final IntegerPreference preference = new IntegerPreference(PREF_KEY, 3);
		// Act + Assert:
		assertThat(preference.getFromPreferences(preferences), is(3));
		preference.updateValue(2017);
		assertThat(preference.putIntoPreferences(preferences), is(any(Boolean.class)));
		preference.invalidate();
		assertThat(preference.getFromPreferences(preferences), is(2017));
	}
}