/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BooleanPreferenceTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Boolean";

	@Test public void testInstantiation() {
		// Act:
		final BooleanPreference preference = new BooleanPreference(PREF_KEY, true);
		// Assert:
		assertThat(preference.getKey(), is(PREF_KEY));
		assertThat(preference.getValue(), is(Boolean.TRUE));
		assertThat(preference.getDefaultValue(), is(Boolean.TRUE));
	}

	@Test public void testPersistence() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:boolean", Context.MODE_PRIVATE);
		final BooleanPreference preference = new BooleanPreference(PREF_KEY, true);
		// Act + Assert:
		assertThat(preference.getFromPreferences(preferences), is(Boolean.TRUE));
		preference.updateValue(false);
		assertThat(preference.putIntoPreferences(preferences), is(any(Boolean.class)));
		preference.invalidate();
		assertThat(preference.getFromPreferences(preferences), is(Boolean.FALSE));
	}
}