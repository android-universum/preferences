/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link SharedPreference} implementation that may be used to persist an {@link Enum} value via
 * {@link SharedPreferences}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see BooleanPreference
 * @see IntegerPreference
 * @see LongPreference
 * @see StringPreference
 * @see FloatPreference
 *
 * @param <E> Type of the enum implementation of which value should be persisted.
 */
public final class EnumPreference<E extends Enum> extends SharedPreference<E> {

	/**
	 * Creates a new instance of EnumPreference with the specified <var>key</var> and <var>defValue</var>.
	 *
	 * @see SharedPreference#SharedPreference(String, Object)
	 */
	public EnumPreference(@NonNull final String key, @NonNull final E defValue) {
		super(key, defValue);
	}

	/**
	 */
	@Override @CheckResult protected boolean onPutIntoPreferences(@NonNull final SharedPreferences preferences) {
		return preferences.edit().putString(key, value == null ? "" : value.name()).commit();
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override @Nullable
	protected E onGetFromPreferences(@NonNull final SharedPreferences preferences) {
		final String enumName = preferences.getString(key, defaultValue.name());
		return TextUtils.isEmpty(enumName) ? null : (E) E.valueOf(defaultValue.getClass(), enumName);
	}
}