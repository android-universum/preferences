/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

/**
 * A {@link SharedPreference} implementation that may be used to persist a {@link Long} value via
 * {@link SharedPreferences}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see BooleanPreference
 * @see IntegerPreference
 * @see FloatPreference
 * @see StringPreference
 * @see EnumPreference
 */
public final class LongPreference extends SharedPreference<Long> {

	/**
	 * Creates a new instance of LongPreference with the specified <var>key</var> and <var>defValue</var>.
	 *
	 * @see SharedPreference#SharedPreference(String, Object)
	 */
	public LongPreference(@NonNull final String key, @NonNull final Long defValue) {
		super(key, defValue);
	}

	/**
	 */
	@Override @CheckResult protected boolean onPutIntoPreferences(@NonNull final SharedPreferences preferences) {
		return preferences.edit().putLong(key, value).commit();
	}

	/**
	 */
	@Override @NonNull protected Long onGetFromPreferences(@NonNull final SharedPreferences preferences) {
		return preferences.getLong(key, defaultValue);
	}
}