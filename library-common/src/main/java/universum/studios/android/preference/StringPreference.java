/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link SharedPreference} implementation that may be used to persist a {@link String} value via
 * {@link SharedPreferences}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see BooleanPreference
 * @see IntegerPreference
 * @see LongPreference
 * @see FloatPreference
 * @see EnumPreference
 */
public final class StringPreference extends SharedPreference<String> {

	/**
	 * Creates a new instance of StringPreference with the specified <var>key</var> and <var>defValue</var>.
	 *
	 * @see SharedPreference#SharedPreference(String, Object)
	 */
	public StringPreference(@NonNull final String key, @Nullable final String defValue) {
		super(key, defValue);
	}

	/**
	 */
	@Override @CheckResult protected boolean onPutIntoPreferences(@NonNull final SharedPreferences preferences) {
		return preferences.edit().putString(key, value).commit();
	}

	/**
	 */
	@Override @Nullable protected String onGetFromPreferences(@NonNull final SharedPreferences preferences) {
		return preferences.getString(key, defaultValue);
	}
}