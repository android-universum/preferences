/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class PreferencesManagerTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";

	private final SharedPreferences.OnSharedPreferenceChangeListener SHARED_PREFERENCE_LISTENER = (sharedPreferences, key) -> {};

	@Test public void testInstantiation() {
		// Arrange:
		final String packageName = context().getPackageName();
		// Act:
		final PreferencesManager manager = new PreferencesManager(context());
		// Assert:
		assertThat(manager.getContext(), is(notNullValue()));
		assertThat(manager.getSharedPreferencesName(), is(packageName + "_preferences"));
		assertThat(manager.getSharedPreferencesMode(), is(SharedPreferencesPolicies.MODE_PRIVATE));
		assertThat(
				manager.getSharedPreferences(),
				is(context().getSharedPreferences(
						packageName + "_preferences",
						SharedPreferencesPolicies.MODE_PRIVATE
				))
		);
		assertThat(manager.isCachingEnabled(), is(false));
	}

	@Test public void testSharedPreferencesName() {
		// Arrange:
		final String packageName = context().getPackageName();
		final PreferencesManager manager = new PreferencesManager(context());
		// Act:
		manager.setSharedPreferencesName(packageName + ":test_preferences");
		// Assert:
		assertThat(manager.getSharedPreferencesName(), is(packageName + ":test_preferences"));
		assertThat(
				manager.getSharedPreferences(),
				is(context().getSharedPreferences(
						packageName + ":test_preferences",
						SharedPreferencesPolicies.MODE_PRIVATE
				))
		);
	}

	@Test public void testSharedPreferencesNameWithNullValue() {
		// Arrange:
		final String packageName = context().getPackageName();
		final PreferencesManager manager = new PreferencesManager(context());
		manager.setSharedPreferencesName(packageName + ":test_preferences");
		// Act:
		manager.setSharedPreferencesName(null);
		// Assert:
		assertThat(manager.getSharedPreferencesName(), is(SharedPreferencesPolicies.defaultPreferencesName(context())));
		assertThat(
				manager.getSharedPreferences(),
				is(context().getSharedPreferences(
						SharedPreferencesPolicies.defaultPreferencesName(context()),
						SharedPreferencesPolicies.MODE_PRIVATE
				))
		);
	}

	@Test public void testSharedPreferencesMode() {
		// Arrange:
		final String packageName = context().getPackageName();
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		manager.setSharedPreferencesMode(SharedPreferencesPolicies.MODE_PRIVATE | SharedPreferencesPolicies.MODE_APPEND);
		assertThat(manager.getSharedPreferencesMode(), is(SharedPreferencesPolicies.MODE_PRIVATE | SharedPreferencesPolicies.MODE_APPEND));
		assertThat(
				manager.getSharedPreferences(),
				is(context().getSharedPreferences(
						packageName + "_preferences",
						SharedPreferencesPolicies.MODE_PRIVATE | SharedPreferencesPolicies.MODE_APPEND
				))
		);
	}

	@Test public void testRegisterUnregisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		manager.registerOnSharedPreferenceChangeListener(SHARED_PREFERENCE_LISTENER);
		manager.unregisterOnSharedPreferenceChangeListener(SHARED_PREFERENCE_LISTENER);
	}

	@Test public void testKey() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.key(android.R.string.ok), is("OK"));
	}

	@Test public void testContains() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.contains(PREF_KEY), is(false));
		assertThat(manager.putString(PREF_KEY, "NewValue"), is(true));
		assertThat(manager.contains(PREF_KEY), is(true));
	}

	@Test public void testStringPersitence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getString(PREF_KEY, "DefaultValue"), is("DefaultValue"));
		assertThat(manager.putString(PREF_KEY, "NewValue"), is(true));
		assertThat(manager.getString(PREF_KEY, "DefaultValue"), is("NewValue"));
	}

	@Test public void testStringSetPersistence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getStringSet(PREF_KEY, null), is(nullValue()));
		final Set<String> set = new HashSet<>(1);
		set.add("NewValue");
		assertThat(manager.putStringSet(PREF_KEY, set), is(true));
		assertThat(manager.getStringSet(PREF_KEY, null), is(set));
	}

	@Test public void testIntPersistence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getInt(PREF_KEY, -1), is(-1));
		assertThat(manager.putInt(PREF_KEY, 1), is(true));
		assertThat(manager.getInt(PREF_KEY, -1), is(1));
	}

	@Test public void testFloatPersistence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getFloat(PREF_KEY, 0.0f), is(0.0f));
		assertThat(manager.putFloat(PREF_KEY, 0.05f), is(true));
		assertThat(manager.getFloat(PREF_KEY, 0.0f), is(0.05f));
	}

	@Test public void testLongPersistence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getLong(PREF_KEY, 100L), is(100L));
		assertThat(manager.putLong(PREF_KEY, -2000L), is(true));
		assertThat(manager.getLong(PREF_KEY, 0L), is(-2000L));
	}

	@Test public void testBooleanPersistence() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getBoolean(PREF_KEY, true), is(true));
		assertThat(manager.putBoolean(PREF_KEY, false), is(true));
		assertThat(manager.getBoolean(PREF_KEY, true), is(false));
	}

	@Test public void testRemove() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.putString(PREF_KEY, "NewValue"), is(true));
		assertThat(manager.remove(PREF_KEY), is(true));
		assertThat(manager.contains(PREF_KEY), is(false));
	}

	@Test public void testRemoveAll() {
		// Arrange:
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.removeAll(), is(0));
		assertThat(manager.putString(PREF_KEY, "NewValue"), is(true));
		assertThat(manager.removeAll(), is(1));
		assertThat(manager.contains(PREF_KEY), is(false));
	}

	@Test public void testPreferencePersistence() {
		// Arrange:
		final SharedPreference<String> preference = new StringPreference(PREF_KEY, "DefaultValue");
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.getPreference(preference), is("DefaultValue"));
		assertThat(manager.putPreference(preference, "UpdatedValue"), is(true));
		assertThat(manager.getPreference(preference), is("UpdatedValue"));
	}

	@Test public void testContainsPreference() {
		// Arrange:
		final StringPreference preference = new StringPreference(PREF_KEY, "DefaultValue");
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.containsPreference(preference), is(false));
		assertThat(manager.putPreference(preference, "UpdatedValue"), is(true));
		assertThat(manager.containsPreference(preference), is(true));
		assertThat(manager.removePreference(preference), is(true));
		assertThat(manager.containsPreference(preference), is(false));
	}

	@Test public void testRemovePreference() {
		// Arrange:
		final SharedPreference<String> preference = new StringPreference(PREF_KEY, "DefaultValue");
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		assertThat(manager.putPreference(preference, "UpdatedValue"), is(true));
		assertThat(manager.removePreference(preference), is(true));
		assertThat(manager.getPreference(preference), is("DefaultValue"));
	}

	@Test public void testCaching() {
		// Arrange:
		final SharedPreference<Boolean> preference = new BooleanPreference("PREFERENCE.Boolean", true);
		final PreferencesManager manager = new PreferencesManager(context());
		// Act + Assert:
		manager.setCachingEnabled(true);
		assertThat(manager.isCachingEnabled(), is(true));
		assertThat(manager.putPreference(preference, false), is(true));
		assertThat(manager.getPreference(preference), is(false));
		manager.setCachingEnabled(false);
		assertThat(manager.putPreference(preference, true), is(true));
		assertThat(manager.getPreference(preference), is(true));
	}
}