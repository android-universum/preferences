/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * Manager which implements {@link SharedPreferencesFacade} along with {@link SharedPreferencesProvider}
 * in order to provide a simple API for putting and obtaining of values persisted within {@link SharedPreferences}.
 *
 * <h3>Shared preferences</h3>
 * The PreferencesManager is closely associated with {@link SharedPreference}. Any implementation of
 * SharedPreference class may be used to represent a concrete preference value to be persisted in
 * shared preferences. Methods listed below may be used to put, get or remove a desired shared preference
 * into or from preferences managed by the preferences manager:
 * <ul>
 * <li>{@link #putPreference(SharedPreference, Object)}</li>
 * <li>{@link #getPreference(SharedPreference)}</li>
 * <li>{@link #containsPreference(SharedPreference)}</li>
 * <li>{@link #removePreference(SharedPreference)}</li>
 * </ul>
 *
 * <h3>Sample implementation</h3>
 * <pre>
 * public final class AppPreferences extends PreferencesManager {
 *
 *      private static final String KEY_PREFIX = BuildConfig.APPLICATION_ID = ".PREFERENCE.";
 *      private static final String KEY_APP_FIRST_LAUNCH = KEY_PREFIX + "AppFirstLaunch";
 *
 *      public AppPreferences(@NonNull Context context) {
 *          super(context);
 *      }
 *
 *      public void setAppFirstLaunch(boolean firstLaunch) {
 *          putBoolean(KEY_APP_FIRST_LAUNCH, firstLaunch);
 *      }
 *
 *      public boolean isAppFirstLaunch() {
 *          return getBoolean(KEY_APP_FIRST_LAUNCH, true);
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class PreferencesManager implements SharedPreferencesFacade, SharedPreferencesProvider {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "PreferencesManager";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Context with which has been this manager created.
	 */
	private final Context context;

	/**
	 * File name of shared preferences managed by this manager.
	 */
	private String preferencesName;

	/**
	 * File mode of shared preferences managed by this manager.
	 */
	@SharedPreferencesPolicies.Mode private int preferencesMode = SharedPreferencesPolicies.MODE_PRIVATE;

	/**
	 * Facade to which is this manager delegating all put/get/remove requests.
	 */
	private SimpleSharedPreferencesFacade preferencesFacade;

	/**
	 * Flag indicating whether caching of the actual values of each shared preference is enabled.
	 * This means that, if shared preference holds actual value which is same as in shared preferences,
	 * parsing of that value will be not performed, instead actual value will be obtained from that
	 * preference object.
	 */
	private boolean cachingEnabled;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of PreferencesManager with the specified <var>context</var>.
	 * <p>
	 * Manager will be created with default name for shared preferences obtained via
	 * {@link SharedPreferencesPolicies#defaultPreferencesName(Context)} and mode set to
	 * {@link SharedPreferencesPolicies#MODE_PRIVATE}.
	 *
	 * @param context Context that is used to obtain instance of {@link SharedPreferences} for the
	 *                new manager via {@link Context#getSharedPreferences(String, int)} for the
	 *                {@code name} and {@code mode} specified via {@link #setSharedPreferencesName(String)}
	 *                and {@link #setSharedPreferencesMode(int)}.
	 */
	public PreferencesManager(@NonNull final Context context) {
		this.context = context;
		this.preferencesName = SharedPreferencesPolicies.defaultPreferencesName(context);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the context with which has been this manager created.
	 *
	 * @return The associated context.
	 */
	@NonNull public final Context getContext() {
		return context;
	}

	/**
	 */
	@Override public void setSharedPreferencesName(@Nullable final String name) {
		this.preferencesName = name == null ? SharedPreferencesPolicies.defaultPreferencesName(context) : name;
		this.preferencesFacade = null;
	}

	/**
	 */
	@Override @NonNull public final String getSharedPreferencesName() {
		return preferencesName;
	}

	/**
	 */
	@Override public void setSharedPreferencesMode(@SharedPreferencesPolicies.Mode final int mode) {
		this.preferencesMode = mode;
		this.preferencesFacade = null;
	}

	/**
	 */
	@Override @SharedPreferencesPolicies.Mode public final int getSharedPreferencesMode() {
		return preferencesMode;
	}

	/**
	 */
	@Override @NonNull public final SharedPreferences getSharedPreferences() {
		this.ensurePreferencesFacade();
		return preferencesFacade.getPreferences();
	}

	/**
	 * Ensures that the {@link #preferencesFacade} for {@link SharedPreferences} managed by this
	 * manager is initialized for {@link #preferencesName} and {@link #preferencesMode}.
	 */
	private void ensurePreferencesFacade() {
		if (preferencesFacade == null) {
			this.preferencesFacade = new SimpleSharedPreferencesFacade(
					context.getSharedPreferences(
							preferencesName,
							preferencesMode
					)
			);
		}
	}

	/**
	 * Enables/disables the caching {@link universum.studios.android.preference.SharedPreference}'s values.
	 *
	 * @param enabled {@code True} to enable caching, {@code false} to disable.
	 *
	 * @see #isCachingEnabled()
	 */
	public final void setCachingEnabled(final boolean enabled) {
		this.cachingEnabled = enabled;
	}

	/**
	 * Returns flag indicating whether the caching of {@link universum.studios.android.preference.SharedPreference}'s
	 * values is enabled or not.
	 *
	 * @return {@code True} if caching is enabled, {@code false} otherwise.
	 *
	 * @see #setCachingEnabled(boolean)
	 */
	public final boolean isCachingEnabled() {
		return cachingEnabled;
	}

	/**
	 * Returns a string key for the specified <var>resId</var>.
	 *
	 * @param resId Resource id of the desired preference key.
	 * @return String key obtained from the context specified for this manager.
	 */
	@NonNull public final String key(@StringRes final int resId) {
		return context.getString(resId);
	}

	/**
	 */
	@Override public void registerOnSharedPreferenceChangeListener(@NonNull final SharedPreferences.OnSharedPreferenceChangeListener listener) {
		this.ensurePreferencesFacade();
		preferencesFacade.registerOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public void unregisterOnSharedPreferenceChangeListener(@NonNull final SharedPreferences.OnSharedPreferenceChangeListener listener) {
		this.ensurePreferencesFacade();
		preferencesFacade.unregisterOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public boolean contains(@NonNull final String key) {
		this.ensurePreferencesFacade();
		return preferencesFacade.contains(key);
	}

	/**
	 */
	@Override public boolean putString(@NonNull final String key, @Nullable final String value) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putString(key, value);
	}

	/**
	 */
	@Override @Nullable public String getString(@NonNull final String key, @Nullable final String defValue) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getString(key, defValue);
	}

	/**
	 */
	@Override public boolean putStringSet(@NonNull final String key, @Nullable final Set<String> values) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putStringSet(key, values);
	}

	/**
	 */
	@Override @Nullable public Set<String> getStringSet(@NonNull final String key, @Nullable final Set<String> defValues) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getStringSet(key, defValues);
	}

	/**
	 */
	@Override public boolean putInt(@NonNull final String key, final int value) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putInt(key, value);
	}

	/**
	 */
	@Override public int getInt(@NonNull final String key, final int defValue) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getInt(key, defValue);
	}

	/**
	 */
	@Override public boolean putFloat(@NonNull final String key, final float value) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putFloat(key, value);
	}

	/**
	 */
	@Override public float getFloat(@NonNull final String key, final float defValue) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getFloat(key, defValue);
	}

	/**
	 */
	@Override public boolean putLong(@NonNull final String key, final long value) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putLong(key, value);
	}

	/**
	 */
	@Override public long getLong(@NonNull final String key, final long defValue) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getLong(key, defValue);
	}

	/**
	 */
	@Override public boolean putBoolean(@NonNull final String key, final boolean value) {
		this.ensurePreferencesFacade();
		return preferencesFacade.putBoolean(key, value);
	}

	/**
	 */
	@Override public boolean getBoolean(@NonNull final String key, final boolean defValue) {
		this.ensurePreferencesFacade();
		return preferencesFacade.getBoolean(key, defValue);
	}

	/**
	 */
	@Override public boolean remove(@NonNull final String key) {
		this.ensurePreferencesFacade();
		return preferencesFacade.remove(key);
	}

	/**
	 */
	@Override public int removeAll() {
		this.ensurePreferencesFacade();
		return preferencesFacade.removeAll();
	}

	/**
	 * Persists the given <var>value</var> for the specified <var>preference</var> into {@link SharedPreferences}
	 * that are managed by this manager.
	 *
	 * @param preference Preference for which to persist the value.
	 * @param value      The value to be persisted.
	 * @param <T>        Type of the value associated with the preference.
	 * @return {@code True} if put has been successful, {@code false} otherwise.
	 *
	 * @see #getPreference(SharedPreference)
	 * @see #containsPreference(SharedPreference)
	 * @see #removePreference(SharedPreference)
	 * @see SharedPreference#updateValue(Object)
	 * @see SharedPreference#putIntoPreferences(SharedPreferences)
	 */
	public final <T> boolean putPreference(@NonNull final SharedPreference<T> preference, @Nullable final T value) {
		this.ensurePreferencesFacade();
		final boolean result = preference.updateValue(value).putIntoPreferences(preferencesFacade.getPreferences());
		if (!cachingEnabled) {
			preference.invalidate();
		}
		return result;
	}

	/**
	 * Obtains the value for the given <var>preference</var> from {@link SharedPreferences} that are
	 * managed by this manager.
	 *
	 * @param preference Preference for which to obtain its associated value.
	 * @param <T>        Type of the value associated with the preference.
	 * @return Value associated with the preference. May be {@code null} if there is no value persisted
	 * for the preference yet.
	 *
	 * @see #putPreference(SharedPreference, Object)
	 * @see #contains(String)
	 * @see SharedPreference#getFromPreferences(SharedPreferences)
	 */
	@Nullable public final <T> T getPreference(@NonNull final SharedPreference<T> preference) {
		this.ensurePreferencesFacade();
		final T value = preference.getFromPreferences(preferencesFacade.getPreferences());
		if (!cachingEnabled) {
			preference.invalidate();
		}
		return value;
	}

	/**
	 * Checks whether there is value associated with the specified <var>preference</var> contained
	 * within {@link SharedPreferences} that are managed by this manager.
	 *
	 * @param preference The desired preference of which value's presence to check.
	 * @return {@code True} if there is value contained for key of the specified preference,
	 * {@code false} otherwise.
	 *
	 * @see #putPreference(SharedPreference, Object)
	 * @see #getPreference(SharedPreference)
	 * @see #removePreference(SharedPreference)
	 */
	public final boolean containsPreference(@NonNull final SharedPreference preference) {
		return contains(preference.getKey());
	}

	/**
	 * Removes a value for the specified <var>preference</var> from {@link SharedPreferences} that
	 * are managed by this manager.
	 *
	 * @param preference The preference for which to remove its associated value.
	 * @return {@code True} if removal has been successful, {@code false} otherwise.
	 *
	 * @see #putPreference(SharedPreference, Object)
	 * @see #getPreference(SharedPreference)
	 * @see #containsPreference(SharedPreference)
	 */
	public final boolean removePreference(@NonNull final SharedPreference preference) {
		return remove(preference.getKey());
	}

	/*
	 * Inner classes ===============================================================================
	 */
}