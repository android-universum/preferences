Preferences-Manager
===============

This module contains a simple **manager** which extends`SimpleSharedPreferencesFacade` in order to 
provide some additional functionality like obtaining of **name** and **file mode** for the manager
`SharedPreferences` and also to support usage of `SharedPreference` implementations for persisting
common, collection based or custom values in shared preferences.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apreferences/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apreferences/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:preferences-manager:${DESIRED_VERSION}@aar"

_depends on:_
[preferences-core](https://bitbucket.org/android-universum/preferences/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [PreferencesManager](https://bitbucket.org/android-universum/preferences/src/main/library-manager/src/main/java/universum/studios/android/preference/PreferencesManager.java)