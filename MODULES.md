Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/preferences/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/preferences/src/main/library-core)**
- **[Cache](https://bitbucket.org/android-universum/preferences/src/main/library-cache)**
- **[Crypto](https://bitbucket.org/android-universum/preferences/src/main/library-crypto)**
- **[Manager](https://bitbucket.org/android-universum/preferences/src/main/library-manager)**
- **[Common](https://bitbucket.org/android-universum/preferences/src/main/library-common)**
- **[Collection](https://bitbucket.org/android-universum/preferences/src/main/library-collection)**
