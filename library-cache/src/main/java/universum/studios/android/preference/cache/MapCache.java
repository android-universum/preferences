/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.preference.SharedPreferencesCache;

/**
 * A {@link SharedPreferencesCache} implementation backed by implementation of {@link Map}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
final class MapCache implements SharedPreferencesCache {

    /*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "MapCache";

    /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Map used to store all values available through this cache.
	 */
	private final Map<String, Object> map = new HashMap<>();
	 
	/*
	 * Constructors ================================================================================
	 */
	 
	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public boolean isEmpty() {
		return map.isEmpty();
	}

	/**
	 */
	@Override public boolean contains(@NonNull final String key) {
		return map.containsKey(key);
	}

	/**
	 */
	@Override public boolean putString(@NonNull final String key, @Nullable final String value) {
		this.map.put(key, value);
		return true;
	}

	/**
	 */
	@Override @Nullable public String getString(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (String) map.get(key);
	}

	/**
	 */
	@Override public boolean putStringSet(@NonNull final String key, @Nullable final Set<String> values) {
		this.map.put(key, values);
		return true;
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override @Nullable public Set<String> getStringSet(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (Set<String>) map.get(key);
	}

	/**
	 */
	@Override public boolean putInt(@NonNull final String key, final int value) {
		this.map.put(key, value);
		return true;
	}

	/**
	 */
	@Override public int getInt(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (int) map.get(key);
	}

	/**
	 */
	@Override public boolean putFloat(@NonNull final String key, final float value) {
		this.map.put(key, value);
		return true;
	}

	/**
	 */
	@Override public float getFloat(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (float) map.get(key);
	}

	/**
	 */
	@Override public boolean putLong(@NonNull final String key, final long value) {
		this.map.put(key, value);
		return true;
	}

	/**
	 */
	@Override public long getLong(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (long) map.get(key);
	}

	/**
	 */
	@Override public boolean putBoolean(@NonNull final String key, final boolean value) {
		this.map.put(key, value);
		return true;
	}

	/**
	 */
	@Override public boolean getBoolean(@NonNull final String key) {
		assertContainsOrThrow(key);
		return (boolean) map.get(key);
	}

	/**
	 * Asserts that this cache contains value for the specified <var>key</var>. If there is no value
	 * for the key stored in this cache, a new {@link NotInCacheException} is thrown.
	 *
	 * @param key The of which value's presence to check.
	 */
	private void assertContainsOrThrow(final String key) {
		if (!map.containsKey(key)) throw new NotInCacheException(key);
	}

	/**
	 */
	@Override public boolean evict(@NonNull final String key) {
		if (map.containsKey(key)) {
			this.map.remove(key);
			return true;
		}
		return false;
	}

	/**
	 */
	@Override public int evictAll() {
		final int size = map.size();
		if (size > 0) {
			this.map.clear();
		}
		return size;
	}
	 
	/*
	 * Inner classes ===============================================================================
	 */
}