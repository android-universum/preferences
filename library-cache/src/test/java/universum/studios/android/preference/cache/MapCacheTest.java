/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference.cache;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import universum.studios.android.preference.SharedPreferencesCache;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class MapCacheTest extends TestCase {

	private static final String PREF_KEY_STRING = "PREFERENCE.Key.String";
	private static final String PREF_KEY_STRING_SET = "PREFERENCE.Key.StringSet";
	private static final String PREF_KEY_INTEGER = "PREFERENCE.Key.Integer";
	private static final String PREF_KEY_FLOAT = "PREFERENCE.Key.Float";
	private static final String PREF_KEY_LONG = "PREFERENCE.Key.Long";
	private static final String PREF_KEY_BOOLEAN = "PREFERENCE.Key.Boolean";

	private static final String[] PREF_KEYS = {
			PREF_KEY_STRING,
			PREF_KEY_STRING_SET,
			PREF_KEY_INTEGER,
			PREF_KEY_FLOAT,
			PREF_KEY_LONG,
			PREF_KEY_BOOLEAN
	};

	@Test public void testInstantiation() {
		// Act:
		final SharedPreferencesCache cache = new MapCache();
		// Assert:
		assertThat(cache.isEmpty(), is(true));
	}

	@Test public void testIsEmpty() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.isEmpty(), is(true));
		cache.putBoolean(PREF_KEY_BOOLEAN, false);
		assertThat(cache.isEmpty(), is(false));
		cache.evictAll();
		assertThat(cache.isEmpty(), is(true));
	}

	@Test public void testContains() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		final Collection<String> exceptKeys = mutableCollectionFrom(PREF_KEYS);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #1:
		assertThat(cache.putString(PREF_KEY_STRING, "pref.value"), is(true));
		exceptKeys.remove(PREF_KEY_STRING);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #2:
		assertThat(cache.putStringSet(PREF_KEY_STRING_SET, new HashSet<String>(0)), is(true));
		exceptKeys.remove(PREF_KEY_STRING_SET);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #3:
		assertThat(cache.putInt(PREF_KEY_INTEGER, 11), is(true));
		exceptKeys.remove(PREF_KEY_INTEGER);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #4:
		assertThat(cache.putFloat(PREF_KEY_FLOAT, 0.25f), is(true));
		exceptKeys.remove(PREF_KEY_FLOAT);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #5:
		assertThat(cache.putLong(PREF_KEY_LONG, 100L), is(true));
		exceptKeys.remove(PREF_KEY_LONG);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
		// Case #6:
		assertThat(cache.putBoolean(PREF_KEY_BOOLEAN, true), is(true));
		exceptKeys.remove(PREF_KEY_BOOLEAN);
		assertThatCacheContainsValueForKeysExcept(cache, exceptKeys);
	}

	private static Collection<String> mutableCollectionFrom(@NonNull String[] array) {
		final Collection<String> collection = new ArrayList<>(array.length);
		Collections.addAll(collection, array);
		return collection;
	}

	private void assertThatCacheContainsValueForKeysExcept(SharedPreferencesCache cache, Collection<String> exceptKeys) {
		for (final String prefKey : PREF_KEYS) {
			if (exceptKeys.contains(prefKey)) {
				continue;
			}
			assertThat("No value for key(" + prefKey + ") is stored in the cache!", cache.contains(prefKey), is(true));
		}
	}

	@Test public void testContainsOnEmptyCache() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		for (final String PREF_KEY : PREF_KEYS) {
			assertThat(cache.contains(PREF_KEY), is(false));
		}
	}

	@Test public void testString() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.putString(PREF_KEY_STRING, "pref.value"), is(true));
		assertThat(cache.getString(PREF_KEY_STRING), is("pref.value"));
		assertThat(cache.putString(PREF_KEY_STRING, "pref.value"), is(true));
		assertThat(cache.getString(PREF_KEY_STRING), is("pref.value"));
		// Test that putting value for another key into cache does not change value for the current key.
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putString(PREF_KEY_STRING + "." + i, "pref.value." + i), is(true));
		}
		assertThat(cache.getString(PREF_KEY_STRING), is("pref.value"));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testPutGetStringWithoutStoredValue() {
		// Act:
		new MapCache().getBoolean(PREF_KEY_STRING);
	}

	@Test public void testStringSet() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		final Set<String> prefValue1 = new HashSet<>(0);
		final Set<String> prefValue2 = new HashSet<>(1);
		prefValue2.add("pref.value");
		assertThat(cache.putStringSet(PREF_KEY_STRING_SET, prefValue1), is(true));
		assertThat(cache.getStringSet(PREF_KEY_STRING_SET), is(prefValue1));
		assertThat(cache.putStringSet(PREF_KEY_STRING_SET, prefValue2), is(true));
		assertThat(cache.getStringSet(PREF_KEY_STRING_SET), is(prefValue2));
		// Test that putting value for another key into cache does not change value for the current key.
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putStringSet(PREF_KEY_STRING_SET + "." + i, null), is(true));
		}
		assertThat(cache.getStringSet(PREF_KEY_STRING_SET), is(prefValue2));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testStringSetWithoutStoredValue() {
		// Act:
		new MapCache().getBoolean(PREF_KEY_STRING_SET);
	}

	@Test public void testInt() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.putInt(PREF_KEY_INTEGER, 14), is(true));
		assertThat(cache.getInt(PREF_KEY_INTEGER), is(14));
		assertThat(cache.putInt(PREF_KEY_INTEGER, 99), is(true));
		assertThat(cache.getInt(PREF_KEY_INTEGER), is(99));
		// Test that putting value for another key into cache does not change value for the current key.
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putInt(PREF_KEY_INTEGER + "." + i, -100 - i), is(true));
		}
		assertThat(cache.getInt(PREF_KEY_INTEGER), is(99));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testIntWithoutStoredValue() {
		// Act:
		new MapCache().getBoolean(PREF_KEY_INTEGER);
	}

	@Test public void testFloat() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.putFloat(PREF_KEY_FLOAT, 0.5f), is(true));
		assertThat(cache.getFloat(PREF_KEY_FLOAT), is(0.5f));
		assertThat(cache.putFloat(PREF_KEY_FLOAT, 1.5f), is(true));
		assertThat(cache.getFloat(PREF_KEY_FLOAT), is(1.5f));
		// Test that putting value for another key into cache does not change value for the current key.
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putFloat(PREF_KEY_FLOAT + "." + i, -0.5f - i), is(true));
		}
		assertThat(cache.getFloat(PREF_KEY_FLOAT), is(1.5f));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testFloatWithoutStoredValue() {
		// Act:
		new MapCache().getBoolean(PREF_KEY_FLOAT);
	}

	@Test public void testLong() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.putLong(PREF_KEY_LONG, 1000L), is(true));
		assertThat(cache.getLong(PREF_KEY_LONG), is(1000L));
		assertThat(cache.putLong(PREF_KEY_LONG, 2000L), is(true));
		assertThat(cache.getLong(PREF_KEY_LONG), is(2000L));
		// Test that putting value for another key into cache does not change value for the current key.
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putLong(PREF_KEY_LONG + "." + i, -1000L - i), is(true));
		}
		assertThat(cache.getLong(PREF_KEY_LONG), is(2000L));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testLongWithoutStoredValue() {
		// Act:
		new MapCache().getLong(PREF_KEY_LONG);
	}

	@Test public void testBoolean() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.putBoolean(PREF_KEY_BOOLEAN, false), is(true));
		assertThat(cache.getBoolean(PREF_KEY_BOOLEAN), is(false));
		assertThat(cache.putBoolean(PREF_KEY_BOOLEAN, true), is(true));
		assertThat(cache.getBoolean(PREF_KEY_BOOLEAN), is(true));
		for (int i = 0; i < 10; i++) {
			assertThat(cache.putBoolean(PREF_KEY_BOOLEAN + "." + i, false), is(true));
		}
		assertThat(cache.getBoolean(PREF_KEY_BOOLEAN), is(true));
	}

	@Test(expected = SharedPreferencesCache.NotInCacheException.class)
	public void testBooleanWithoutStoredValue() {
		// Act:
		new MapCache().getBoolean(PREF_KEY_BOOLEAN);
	}

	@Test public void testEvict() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		cache.putString(PREF_KEY_STRING, "pref.value");
		assertThat(cache.evict(PREF_KEY_STRING), is(true));
		assertThat(cache.contains(PREF_KEY_STRING), is(false));
		cache.putStringSet(PREF_KEY_STRING_SET, null);
		assertThat(cache.evict(PREF_KEY_STRING_SET), is(true));
		assertThat(cache.contains(PREF_KEY_STRING_SET), is(false));
		cache.putInt(PREF_KEY_INTEGER, 1);
		assertThat(cache.evict(PREF_KEY_INTEGER), is(true));
		assertThat(cache.contains(PREF_KEY_INTEGER), is(false));
		cache.putFloat(PREF_KEY_FLOAT, 0.5f);
		assertThat(cache.evict(PREF_KEY_FLOAT), is(true));
		assertThat(cache.contains(PREF_KEY_FLOAT), is(false));
		cache.putLong(PREF_KEY_LONG, 1000L);
		assertThat(cache.evict(PREF_KEY_LONG), is(true));
		assertThat(cache.contains(PREF_KEY_LONG), is(false));
		cache.putBoolean(PREF_KEY_BOOLEAN, false);
		assertThat(cache.evict(PREF_KEY_BOOLEAN), is(true));
		assertThat(cache.contains(PREF_KEY_BOOLEAN), is(false));
	}

	@Test public void testEvictOnEmptyCache() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.evict(PREF_KEY_BOOLEAN), is(false));
		cache.putBoolean(PREF_KEY_BOOLEAN, true);
		cache.evict(PREF_KEY_BOOLEAN);
		assertThat(cache.evict(PREF_KEY_BOOLEAN), is(false));
	}

	@Test public void testEvictAll() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		cache.putString(PREF_KEY_STRING, "pref.value");
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_STRING), is(false));
		cache.putStringSet(PREF_KEY_STRING_SET, null);
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_STRING_SET), is(false));
		cache.putInt(PREF_KEY_INTEGER, 1);
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_INTEGER), is(false));
		cache.putFloat(PREF_KEY_FLOAT, 0.5f);
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_FLOAT), is(false));
		cache.putLong(PREF_KEY_LONG, 1000L);
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_LONG), is(false));
		assertThat(cache.putBoolean(PREF_KEY_BOOLEAN, false), is(true));
		assertThat(cache.evictAll(), is(1));
		assertThat(cache.contains(PREF_KEY_BOOLEAN), is(false));
	}

	@Test public void testEvictAllOnEmptyCache() {
		// Arrange:
		final SharedPreferencesCache cache = new MapCache();
		// Act + Assert:
		assertThat(cache.evictAll(), is(0));
		assertThat(cache.putBoolean(PREF_KEY_BOOLEAN, true), is(true));
		assertThat(cache.evict(PREF_KEY_BOOLEAN), is(true));
		assertThat(cache.evictAll(), is(0));
	}
}