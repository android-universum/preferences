Preferences-Cache
===============

This module contains a simple factory which provides **common** implementations of `SharedPreferencesCache`
which may be used in association with `CryptoSharedPreferences`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apreferences/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apreferences/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:preferences-cache:${DESIRED_VERSION}@aar"

_depends on:_
[preferences-core](https://bitbucket.org/android-universum/preferences/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SharedPreferenceCaches](https://bitbucket.org/android-universum/preferences/src/main/library-cache/src/main/java/universum/studios/android/preference/cache/SharedPreferenceCaches.java)