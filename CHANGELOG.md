Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 2.x ##

### [2.2.2](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 12.03.2020

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [2.2.1](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [2.2.0](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 17.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [2.1.3](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 03.06.2018

- Small updates and improvements.

### [2.1.2](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 14.11.2017

- **Updated** to the latest **to date available** dependencies. **Mainly** to the _Android Platform 27 (8.1)_.

### [2.1.1](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 27.07.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### [2.1.0](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 21.05.2017

- Stability improvements. 

### [2.0.3](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 29.04.2017

- Removed **deprecated** elements.

### [2.0.2](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 22.04.2017

- `SharedPreferencesPolicy` class has been deprecated and replaced by `SharedPreferencesPolicies`.

### [2.0.1](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 20.04.2017

- `CryptoSharedPreferences` now properly delegate **encryption** and **decryption** for `Set<String>` value
  and some additional fixes for the crypto preferences including wrong checking of **default** value
  whenever one of `get...(...)` methods has been called.

### [2.0.0](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 09.04.2017

- Removed all deprecated elements and methods from all previous **beta** releases.

### [2.0.0-beta2](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 05.04.2017

- Refactored implementation of `PreferencesManager`. Now only implements `SharedPreferencesFacade`
  along with `SharedPreferencesProvider` and may be created via simple constructor taking only `Context`
  as parameter. Desired configuration related to preferences file name and mode may be specified via
  `setSharedPreferencesName(String)` and `setSharedPreferencesMode(int)`.

### [2.0.0-beta1](https://bitbucket.org/android-universum/preferences/wiki/version/2.x) ###
> 02.04.2017

- Added `SharedPreferencesFacade`, `SimpleSharedPreferencesFacade` and `SharedPreferencesWrapper`
  into **[preferences-core](https://github.com/universum-studios/android_preferences/MODULES.md)**
  library module.
- `PreferencesManager` has been refactored in order to extend `SimpleSharedPreferencesFacade` and has
  been moved into separate library module named **[preferences-manager](https://github.com/universum-studios/android_preferences/MODULES.md)**.
- `SharedPreference` has been refactored along with all its direct implementations provided by the
  library via **[preferences-common](https://github.com/universum-studios/android_preferences/MODULES.md)**
  and **[preferences-collection](https://github.com/universum-studios/android_preferences/MODULES.md)**
  modules.

## [Version 1.x](https://bitbucket.org/android-universum/preferences/wiki/version/1.x) ##