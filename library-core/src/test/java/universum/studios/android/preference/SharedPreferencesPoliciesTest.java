/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SharedPreferencesPoliciesTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		SharedPreferencesPolicies.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<SharedPreferencesPolicies> constructor = SharedPreferencesPolicies.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testDefaultConstants() {
		// Assert:
		assertThat(SharedPreferencesPolicies.DEFAULT_PREFERENCES_NAME_SUFFIX, is("_preferences"));
	}

	@Test public void testFileModes() {
		// Assert:
		assertThat(SharedPreferencesPolicies.MODE_PRIVATE, is(Context.MODE_PRIVATE));
		assertThat(SharedPreferencesPolicies.MODE_APPEND, is(Context.MODE_APPEND));
	}

	@Test public void testDefaultPreferencesName() {
		// Act:
		final String name = SharedPreferencesPolicies.defaultPreferencesName(context());
		// Assert:
		assertThat(name, is(context().getPackageName() + SharedPreferencesPolicies.DEFAULT_PREFERENCES_NAME_SUFFIX));
	}

	@Test public void testPreferencesName() {
		// Act:
		final String name = SharedPreferencesPolicies.preferencesName(context(), ":test_preferences");
		// Assert:
		assertThat(name, is(context().getPackageName() + ":test_preferences"));
	}
}