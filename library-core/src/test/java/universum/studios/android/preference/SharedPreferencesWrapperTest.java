/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Tests only delegation for all provided methods by {@link SharedPreferencesWrapper} to the
 * {@link SharedPreferences} instance wrapped by instance of such wrapper.
 *
 * @author Martin Albedinsky
 */
@SuppressLint("CommitPrefEdits")
public final class SharedPreferencesWrapperTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";

	@Test public void testGetWrappedPreferences() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		final SharedPreferences preferences = wrapper.getWrappedPreferences();
		// Assert:
		assertThat(preferences, is(mockPreferences));
	}

	@Test public void testRegisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.registerOnSharedPreferenceChangeListener(mockListener);
		// Assert:
		verify(mockPreferences).registerOnSharedPreferenceChangeListener(mockListener);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testUnregisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.unregisterOnSharedPreferenceChangeListener(mockListener);
		// Assert:
		verify(mockPreferences).unregisterOnSharedPreferenceChangeListener(mockListener);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetAll() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getAll();
		// Assert:
		verify(mockPreferences).getAll();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testContains() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.contains(PREF_KEY);
		// Assert:
		verify(mockPreferences).contains(PREF_KEY);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetString() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getString(PREF_KEY, "Slovakia");
		// Assert:
		verify(mockPreferences).getString(PREF_KEY, "Slovakia");
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetStringSet() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getStringSet(PREF_KEY, null);
		// Assert:
		verify(mockPreferences).getStringSet(PREF_KEY, null);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetInt() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getInt(PREF_KEY, 12);
		// Assert:
		verify(mockPreferences).getInt(PREF_KEY, 12);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetLong() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getLong(PREF_KEY, 1000L);
		// Assert:
		verify(mockPreferences).getLong(PREF_KEY, 1000L);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetFloat() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getFloat(PREF_KEY, 0.55f);
		// Assert:
		verify(mockPreferences).getFloat(PREF_KEY, 0.55f);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetBoolean() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.getBoolean(PREF_KEY, false);
		// Assert:
		verify(mockPreferences).getBoolean(PREF_KEY, false);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testEdit() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesWrapper wrapper = new SharedPreferencesWrapper(mockPreferences);
		// Act:
		wrapper.edit();
		// Assert:
		verify(mockPreferences).edit();
		verifyNoMoreInteractions(mockPreferences);
	}
}