/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Albedinsky
 */
public final class SharedPreferenceTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Impl";
	private static final String PREF_DEF_VALUE = "defValue";

	@Test public void testInstantiation() {
		// Act:
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		// Assert:
		assertThat(preference.getKey(), is(PREF_KEY));
		assertThat(preference.getValue(), is(PREF_DEF_VALUE));
		assertThat(preference.getDefaultValue(), is(PREF_DEF_VALUE));
	}

	@Test public void testInstantiationWithInvalidKeyString() {
		try {
			// Act:
			new PreferenceImpl("", "defValue");
			throw new AssertionError("No exception thrown.");
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(e.getMessage(), is("Preference key cannot be empty."));
		}
	}

	@Test public void testUpdateAndGetValue() {
		// Arrange:
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		// Act + Assert:
		// Case #1:
		assertThat(preference.updateValue("newValue"), is(preference));
		assertThat(preference.getValue(), is("newValue"));
		// Case #2:
		assertThat(preference.updateValue("newValue"), is(preference));
		assertThat(preference.getValue(), is("newValue"));
		// Case #3:
		assertThat(preference.updateValue(null), is(preference));
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Test public void testInvalidate() {
		// Arrange:
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		preference.updateValue("newValue");
		// Act:
		preference.invalidate();
		// Assert:
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Test public void testGetFromPreferences() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:base", Context.MODE_PRIVATE);
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		// Act + Assert:
		assertThat(preference.getFromPreferences(preferences), is(PREF_DEF_VALUE));
		assertThat(preference.getValue(), is(PREF_DEF_VALUE));
		// Test again to cover case when the value of preference has been already obtained.
		assertThat(preference.getFromPreferences(preferences), is(PREF_DEF_VALUE));
		assertThat(preference.getValue(), is(PREF_DEF_VALUE));
	}

	@Test public void testPutIntoPreferences() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:base", Context.MODE_PRIVATE);
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		// Act + Assert:
		assertThat(preference.updateValue("newValue"), is(preference));
		assertThat(preference.putIntoPreferences(preferences), is(true));
		assertThat(preference.getValue(), is("newValue"));
	}

	@Test public void testCreateOnChangeListener() {
		// Arrange:
		final SharedPreference.PreferenceChangeCallback<String> mockCallback = mock(PreferenceChangeStringCallback.class);
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		// Act:
		final SharedPreferences.OnSharedPreferenceChangeListener listener = preference.createOnChangeListener(mockCallback);
		// Assert:
		assertThat(listener, is(notNullValue()));
	}

	@Test public void testOnChangeListenerCallback() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:base", Context.MODE_PRIVATE);
		final SharedPreference.PreferenceChangeCallback<String> mockCallback = mock(PreferenceChangeStringCallback.class);
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		final SharedPreferences.OnSharedPreferenceChangeListener listener = preference.createOnChangeListener(mockCallback);
		// Act:
		listener.onSharedPreferenceChanged(preferences, preference.getKey());
		// Assert:
		verify(mockCallback).onPreferenceChanged(preference);
	}

	@Test public void testOnChangeListenerCallbackWithNotPreferenceKey() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:base", Context.MODE_PRIVATE);
		final SharedPreference.PreferenceChangeCallback<String> mockCallback = mock(PreferenceChangeStringCallback.class);
		final SharedPreference<String> preference = new PreferenceImpl(PREF_KEY, PREF_DEF_VALUE);
		final SharedPreferences.OnSharedPreferenceChangeListener listener = preference.createOnChangeListener(mockCallback);
		// Act:
		listener.onSharedPreferenceChanged(preferences, "NotPreferenceKey");
		// Assert:
		verify(mockCallback, times(0)).onPreferenceChanged(preference);
	}

	private static final class PreferenceImpl extends SharedPreference<String> {

		PreferenceImpl(@NonNull final String key, @Nullable final String defValue) {
			super(key, defValue);
		}

		@Override protected boolean onPutIntoPreferences(@NonNull final SharedPreferences preferences) {
			preferences.edit().putString(key, value).commit();
			return true;
		}

		@Override @Nullable protected String onGetFromPreferences(@NonNull final SharedPreferences preferences) {
			return preferences.getString(key, defaultValue);
		}
	}

	private interface PreferenceChangeStringCallback extends SharedPreference.PreferenceChangeCallback<String> {}
}