/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SharedPreferencesFactoryTest extends AndroidTestCase {

	@Test public void testDefaultInstance() {
		// Assert:
		assertThat(SharedPreferencesFactory.DEFAULT.createPreferences(context()), is(notNullValue()));
	}

	@Test public void testSimpleFactoryWithName() {
		// Arrange:
		final SharedPreferencesFactory factory = new SharedPreferencesFactory.SimpleFactory(":preferences");
		// Act:
		final SharedPreferences preferences = factory.createPreferences(context());
		// Assert
		assertThat(preferences, is(notNullValue()));
		assertThat(factory.createPreferences(context()), is(preferences));
	}

	@Test public void testSimpleFactoryWithNameAndMode() {
		// Arrange:
		final SharedPreferencesFactory factory = new SharedPreferencesFactory.SimpleFactory(
				":preferences",
				SharedPreferencesPolicies.MODE_PRIVATE
		);
		// Act:
		final SharedPreferences preferences = factory.createPreferences(context());
		// Assert:
		assertThat(preferences, is(notNullValue()));
		assertThat(factory.createPreferences(context()), is(preferences));
	}
}