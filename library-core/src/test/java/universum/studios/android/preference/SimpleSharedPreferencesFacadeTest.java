/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests only delegation for all provided methods by {@link SimpleSharedPreferencesFacade} to the
 * {@link SharedPreferences} instance hidden behind instance of such facade.
 *
 * @author Martin Albedinsky
 */
@SuppressLint("CommitPrefEdits")
public final class SimpleSharedPreferencesFacadeTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Key";

	@Test public void testGetPreferences() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SimpleSharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act + Assert:
		assertThat(facade.getPreferences(), is(mockPreferences));
	}

	@Test public void testRegisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.registerOnSharedPreferenceChangeListener(mockListener);
		// Assert:
		verify(mockPreferences).registerOnSharedPreferenceChangeListener(mockListener);
	}

	@Test public void testUnregisterOnSharedPreferenceChangeListener() {
		// Arrange:
		final SharedPreferences.OnSharedPreferenceChangeListener mockListener = mock(SharedPreferences.OnSharedPreferenceChangeListener.class);
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.unregisterOnSharedPreferenceChangeListener(mockListener);
		// Assert:
		verify(mockPreferences).unregisterOnSharedPreferenceChangeListener(mockListener);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testContains() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.contains(PREF_KEY);
		// Assert:
		verify(mockPreferences).contains(PREF_KEY);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutString() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putString(PREF_KEY, "Universe")).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putString(PREF_KEY, "Universe");
		verify(mockPreferences).edit();
		verify(mockEditor).putString(PREF_KEY, "Universe");
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetString() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getString(PREF_KEY, "Slovakia");
		// Assert:
		verify(mockPreferences).getString(PREF_KEY, "Slovakia");
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutStringSet() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putStringSet(PREF_KEY, null)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putStringSet(PREF_KEY, null);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).putStringSet(PREF_KEY, null);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetStringSet() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getStringSet(PREF_KEY, null);
		// Assert:
		verify(mockPreferences).getStringSet(PREF_KEY, null);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutInt() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putInt(PREF_KEY, 14)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putInt(PREF_KEY, 14);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).putInt(PREF_KEY, 14);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetInt() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getInt(PREF_KEY, 12);
		// Assert:
		verify(mockPreferences).getInt(PREF_KEY, 12);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutLong() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putLong(PREF_KEY, 20000L)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putLong(PREF_KEY, 20000L);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).putLong(PREF_KEY, 20000L);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetLong() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getLong(PREF_KEY, 1000L);
		// Assert:
		verify(mockPreferences).getLong(PREF_KEY, 1000L);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutFloat() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putFloat(PREF_KEY, -14.56f)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putFloat(PREF_KEY, -14.56f);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).putFloat(PREF_KEY, -14.56f);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetFloat() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getFloat(PREF_KEY, 0.55f);
		// Assert:
		verify(mockPreferences).getFloat(PREF_KEY, 0.55f);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testPutBoolean() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.putBoolean(PREF_KEY, true)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.putBoolean(PREF_KEY, true);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).putBoolean(PREF_KEY, true);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testGetBoolean() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.getBoolean(PREF_KEY, false);
		// Assert:
		verify(mockPreferences).getBoolean(PREF_KEY, false);
		verifyNoMoreInteractions(mockPreferences);
	}

	@Test public void testRemove() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		when(mockEditor.remove(PREF_KEY)).thenReturn(mockEditor);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.remove(PREF_KEY);
		// Assert:
		verify(mockPreferences).edit();
		verify(mockEditor).remove(PREF_KEY);
		verify(mockEditor).commit();
		verifyNoMoreInteractions(mockPreferences);
	}

	@SuppressWarnings("unchecked")
	@Test public void testRemoveAll() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferences.Editor mockEditor = mock(SharedPreferences.Editor.class);
		when(mockPreferences.edit()).thenReturn(mockEditor);
		final Map<String, Object> values = new HashMap<>(5);
		for (int i = 0; i < 5; i++) {
			final String key = PREF_KEY + "." + i;
			values.put(key, Integer.toString(i));
			when(mockEditor.remove(key)).thenReturn(mockEditor);
		}
		when(mockPreferences.getAll()).thenReturn((Map) values);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.removeAll();
		// Assert:
		verify(mockPreferences, times(values.size())).edit();
		for (int i = 0; i < values.size(); i++) {
			final String key = PREF_KEY + "." + i;
			verify(mockEditor).remove(key);
		}
		verify(mockEditor, times(values.size())).commit();
	}

	@Test public void testRemoveAllWithoutPersistedValues() {
		// Arrange:
		final SharedPreferences mockPreferences = mock(SharedPreferences.class);
		final SharedPreferencesFacade facade = new SimpleSharedPreferencesFacade(mockPreferences);
		// Act:
		facade.removeAll();
		// Assert:
		verify(mockPreferences, times(0)).edit();
	}
}