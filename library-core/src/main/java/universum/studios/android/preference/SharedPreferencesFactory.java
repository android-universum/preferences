/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;

/**
 * Simple interface for factories that may be used to create instances of {@link SharedPreferences}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface SharedPreferencesFactory {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * A {@link SharedPreferencesFactory} which may be used to create default shared preferences.
	 *
	 * @see PreferenceManager#getDefaultSharedPreferences(Context)
	 */
	SharedPreferencesFactory DEFAULT = new SharedPreferencesFactory() {

		/**
		 */
		@Override @NonNull public SharedPreferences createPreferences(@NonNull final Context context) {
			return PreferenceManager.getDefaultSharedPreferences(context);
		}
	};

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of SharedPreferences for the given <var>context</var>.
	 *
	 * @param context Context used to create shared preferences.
	 * @return Instance of SharedPreferences with name and file creation mode specific for implementation
	 * of this factory.
	 *
	 * @see Context#getSharedPreferences(String, int)
	 */
	@NonNull SharedPreferences createPreferences(@NonNull Context context);

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A simple implementation of {@link SharedPreferencesFactory} which may be used to create
	 * instances of {@link SharedPreferences} with a desired name and file creation mode via either
	 * {@link #SimpleFactory(String)} or {@link #SimpleFactory(String, int)}.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 */
	class SimpleFactory implements SharedPreferencesFactory {

		/**
		 * Name for the preferences file.
		 */
		private final String fileName;

		/**
		 * File creation mode for the preferences file.
		 */
		private final int fileMode;

		/**
		 * Creates a new instance of SimpleFactory with the specified <var>fileName</var> and
		 * {@link SharedPreferencesPolicies#MODE_PRIVATE)} as file mode for preferences file.
		 *
		 * @param fileName The desired name for preferences file.
		 *
		 * @see #SimpleFactory(String, int)
		 */
		public SimpleFactory(@NonNull final String fileName) {
			this(fileName, SharedPreferencesPolicies.MODE_PRIVATE);
		}

		/**
		 * Creates a new instance of SimpleFactory with the specified <var>fileName</var> and
		 * <var>fileMode</var> for preferences file.
		 *
		 * @param fileName The desired name for preferences file.
		 * @param fileMode The desired creation mode for preferences file.
		 */
		public SimpleFactory(@NonNull final String fileName, @SharedPreferencesPolicies.Mode final int fileMode) {
			this.fileName = fileName;
			this.fileMode = fileMode;
		}

		/**
		 */
		@Override @NonNull public SharedPreferences createPreferences(@NonNull final Context context) {
			return context.getSharedPreferences(fileName, fileMode);
		}
	}
}