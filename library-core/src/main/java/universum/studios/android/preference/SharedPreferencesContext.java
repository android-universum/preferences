/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import androidx.annotation.Nullable;

/**
 * Interface for contexts that depend on {@link SharedPreferences}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface SharedPreferencesContext {

	/**
	 * Sets an instance of shared preferences to be used by this context.
	 *
	 * @param preferences The desired instance of preferences to be used. May be {@code null} to no
	 *                    use shared preferences.
	 * @see #getSharedPreferences()
	 */
	void setSharedPreferences(@Nullable SharedPreferences preferences);

	/**
	 * Returns the shared preferences instance used by this context.
	 *
	 * @return The associated shared preferences. May be {@code null} if no preferences has been
	 * associated with this context.
	 *
	 * @see #setSharedPreferences(SharedPreferences)
	 */
	@Nullable SharedPreferences getSharedPreferences();
}