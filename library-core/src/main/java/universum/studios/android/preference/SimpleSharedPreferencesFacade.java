/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Simple implementation of {@link SharedPreferencesFacade} which supports simple <var>obtaining</var>
 * and <var>putting</var> of values persisted in {@link SharedPreferences} with which is facade created
 * via {@link #SimpleSharedPreferencesFacade(SharedPreferences)} constructor.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public class SimpleSharedPreferencesFacade implements SharedPreferencesFacade {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleSharedPreferencesFacade";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Instance of SharedPreferences that is used by this facade implementation to manage putting and
	 * obtaining of values into/from preferences in a simple way.
	 */
	private final SharedPreferences preferences;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleSharedPreferencesFacade for the specified <var>preferences</var>.
	 *
	 * @param preferences The instance of shared preferences to hide behind the new facade.
	 */
	public SimpleSharedPreferencesFacade(@NonNull final SharedPreferences preferences) {
		this.preferences = preferences;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the {@link SharedPreferences} that are hidden behind this facade.
	 *
	 * @return The associated preferences.
	 *
	 * @see #SimpleSharedPreferencesFacade(SharedPreferences)
	 */
	@NonNull public final SharedPreferences getPreferences() {
		return preferences;
	}

	/**
	 */
	@Override public void registerOnSharedPreferenceChangeListener(@NonNull final SharedPreferences.OnSharedPreferenceChangeListener listener) {
		this.preferences.registerOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public void unregisterOnSharedPreferenceChangeListener(@NonNull final SharedPreferences.OnSharedPreferenceChangeListener listener) {
		this.preferences.unregisterOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public boolean contains(@NonNull final String key) {
		return preferences.contains(key);
	}

	/**
	 */
	@Override public boolean putString(@NonNull final String key, @Nullable final String value) {
		return preferences.edit().putString(key, value).commit();
	}

	/**
	 */
	@Override @Nullable public String getString(@NonNull final String key, @Nullable final String defValue) {
		return preferences.getString(key, defValue);
	}

	/**
	 */
	@Override public boolean putStringSet(@NonNull final String key, @Nullable final Set<String> values) {
		return preferences.edit().putStringSet(key, values).commit();
	}

	/**
	 */
	@Override @Nullable public Set<String> getStringSet(@NonNull final String key, @Nullable final Set<String> defValues) {
		return preferences.getStringSet(key, defValues);
	}

	/**
	 */
	@Override public boolean putInt(@NonNull final String key, final int value) {
		return preferences.edit().putInt(key, value).commit();
	}

	/**
	 */
	@Override public int getInt(@NonNull final String key, final int defValue) {
		return preferences.getInt(key, defValue);
	}

	/**
	 */
	@Override public boolean putFloat(@NonNull final String key, final float value) {
		return preferences.edit().putFloat(key, value).commit();
	}

	/**
	 */
	@Override public float getFloat(@NonNull final String key, final float defValue) {
		return preferences.getFloat(key, defValue);
	}

	/**
	 */
	@Override public boolean putLong(@NonNull final String key, final long value) {
		return preferences.edit().putLong(key, value).commit();
	}

	/**
	 */
	@Override public long getLong(@NonNull final String key, final long defValue) {
		return preferences.getLong(key, defValue);
	}

	/**
	 */
	@Override public boolean putBoolean(@NonNull final String key, final boolean value) {
		return preferences.edit().putBoolean(key, value).commit();
	}

	/**
	 */
	@Override public boolean getBoolean(@NonNull final String key, final boolean defValue) {
		return preferences.getBoolean(key, defValue);
	}

	/**
	 */
	@Override public boolean remove(@NonNull final String key) {
		return preferences.edit().remove(key).commit();
	}

	/**
	 */
	@Override public int removeAll() {
		final Map<String, ?> values = preferences.getAll();
		int result = 0;
		if (!values.isEmpty()) {
			for (final Map.Entry<String, ?> entry : values.entrySet()) {
				if (preferences.edit().remove(entry.getKey()).commit()) result++;
			}
		}
		return result;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}