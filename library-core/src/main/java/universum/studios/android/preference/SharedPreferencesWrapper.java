/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.SharedPreferences;

import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link SharedPreferences} implementation which may be used to wrap an instance of {@link SharedPreferences}.
 * This wrapper class simply delegates all its calls to the wrapped preferences instance.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public class SharedPreferencesWrapper implements SharedPreferences {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SharedPreferencesWrapper";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Wrapped instance of SharedPreferences to which is this wrapper delegating all its calls.
	 */
	private final SharedPreferences preferences;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SharedPreferencesWrapper in order to wrap the given <var>preferences</var>
	 * instance.
	 *
	 * @param preferences The shared preferences to be wrapped.
	 */
	public SharedPreferencesWrapper(@NonNull final SharedPreferences preferences) {
		this.preferences = preferences;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the wrapped SharedPreferences instance.
	 *
	 * @return Instance of SharedPreferences that are wrapped by this preferences wrapper.
	 */
	@NonNull public final SharedPreferences getWrappedPreferences() {
		return preferences;
	}

	/**
	 */
	@Override public void registerOnSharedPreferenceChangeListener(@NonNull final OnSharedPreferenceChangeListener listener) {
		this.preferences.registerOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public void unregisterOnSharedPreferenceChangeListener(@NonNull final OnSharedPreferenceChangeListener listener) {
		this.preferences.unregisterOnSharedPreferenceChangeListener(listener);
	}

	/**
	 */
	@Override public Map<String, ?> getAll() {
		return preferences.getAll();
	}

	/**
	 */
	@Override public boolean contains(@NonNull final String key) {
		return preferences.contains(key);
	}

	/**
	 */
	@Override @Nullable public String getString(@NonNull final String key, @Nullable final String defValue) {
		return preferences.getString(key, defValue);
	}

	/**
	 */
	@Override @Nullable public Set<String> getStringSet(@NonNull final String key, @Nullable final Set<String> defValues) {
		return preferences.getStringSet(key, defValues);
	}

	/**
	 */
	@Override public int getInt(@NonNull final String key, final int defValue) {
		return preferences.getInt(key, defValue);
	}

	/**
	 */
	@Override public long getLong(@NonNull final String key, final long defValue) {
		return preferences.getLong(key, defValue);
	}

	/**
	 */
	@Override public float getFloat(@NonNull final String key, final float defValue) {
		return preferences.getFloat(key, defValue);
	}

	/**
	 */
	@Override public boolean getBoolean(@NonNull final String key, final boolean defValue) {
		return preferences.getBoolean(key, defValue);
	}

	/**
	 */
	@Override @NonNull public Editor edit() {
		return preferences.edit();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}