/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.preference.PreferenceManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

/**
 * Class that declares policies related to the Android {@code SharedPreferences} API.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class SharedPreferencesPolicies {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Suffix for default name used by the <b>Android</b> for shared preferences.
	 */
	public static final String DEFAULT_PREFERENCES_NAME_SUFFIX = "_preferences";

	/**
	 * Copied flag from {@link Context#MODE_PRIVATE}.
	 */
	public static final int MODE_PRIVATE = Context.MODE_PRIVATE;

	/**
	 * Copied flag from {@link Context#MODE_APPEND}.
	 */
	public static final int MODE_APPEND = Context.MODE_APPEND;

	/**
	 * Defines an annotation for determining allowed file creation modes for {@code SharedPreferences}
	 * file.
	 */
	@IntDef(flag = true, value = {
			MODE_PRIVATE,
			MODE_APPEND
	})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Mode {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private SharedPreferencesPolicies() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a default name for shared preferences for the specified <var>context</var> that is
	 * the same one as created by default by the <b>Android</b> framework.
	 *
	 * @param context The context for which to create preferences name.
	 * @return Preferences name that may be used in association with {@link Context#getSharedPreferences(String, int)}.
	 *
	 * @see #preferencesName(Context, String)
	 * @see PreferenceManager#getDefaultSharedPreferencesName(Context)
	 */
	@NonNull public static String defaultPreferencesName(@NonNull final Context context) {
		return preferencesName(context, DEFAULT_PREFERENCES_NAME_SUFFIX);
	}

	/**
	 * Creates a name for shared preferences with the given <var>nameSuffix</var> for the specified
	 * <var>context</var>.
	 *
	 * @param context    The context for which to create preferences name.
	 * @param nameSuffix Suffix to be added into preferences name.
	 * @return Preferences name that may be used in association with {@link Context#getSharedPreferences(String, int)}.
	 *
	 * @see #defaultPreferencesName(Context)
	 */
	@NonNull public static String preferencesName(@NonNull final Context context, @NonNull final String nameSuffix) {
		return context.getPackageName() + nameSuffix;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}