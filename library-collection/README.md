Preferences-Collection
===============

This module contains implementations of `SharedPreference` which may be used to simplify
accessing of **collection based** values persisted in `SharedPreferences`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apreferences/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apreferences/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:preferences-collection:${DESIRED_VERSION}@aar"

_depends on:_
[preferences-core](https://bitbucket.org/android-universum/preferences/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [CollectionPreference](https://bitbucket.org/android-universum/preferences/src/main/library-collection/src/main/java/universum/studios/android/preference/CollectionPreference.java)
- [ArrayPreference](https://bitbucket.org/android-universum/preferences/src/main/library-collection/src/main/java/universum/studios/android/preference/ArrayPreference.java)