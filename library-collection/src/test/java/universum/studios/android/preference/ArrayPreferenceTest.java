/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.junit.Test;

import java.util.Date;

import universum.studios.android.test.AndroidTestCase;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResourceType")
public final class ArrayPreferenceTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Array";

	@SuppressWarnings("unused")
	private enum TestEnum {
		WINTER, SPRING, SUMMER, AUTUMN
	}

	@Test public void testInstantiation() {
		// Act:
		final ArrayPreference<String> preference = new ArrayPreference<>(PREF_KEY, null);
		// Assert:
		assertThat(preference.getKey(), is(PREF_KEY));
		assertThat(preference.getDefaultValue(), is(nullValue()));
	}

	@Test public void testInstantiationWithUnsupportedType() {
		try {
			// Act:
			new ArrayPreference<>(PREF_KEY, new Date());
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(TextUtils.isEmpty(e.getMessage()), is(false));
			assertThat(e.getMessage(), is("Not an array(Date)."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testResolveArrayClass() {
		// Act + Assert:
		assertEquals(boolean[].class, ArrayPreference.resolveArrayClass(new boolean[]{true}));
		assertEquals(byte[].class, ArrayPreference.resolveArrayClass(new byte[]{0}));
		assertEquals(char[].class, ArrayPreference.resolveArrayClass(new char[]{0}));
		assertEquals(short[].class, ArrayPreference.resolveArrayClass(new short[]{2}));
		assertEquals(int[].class, ArrayPreference.resolveArrayClass(new int[]{1}));
		assertEquals(float[].class, ArrayPreference.resolveArrayClass(new float[]{1.5f}));
		assertEquals(long[].class, ArrayPreference.resolveArrayClass(new long[]{198L}));
		assertEquals(double[].class, ArrayPreference.resolveArrayClass(new double[]{15D}));
		assertEquals(Boolean[].class, ArrayPreference.resolveArrayClass(new Boolean[]{true, null}));
		assertEquals(Byte[].class, ArrayPreference.resolveArrayClass(new Byte[]{0, null}));
		assertEquals(Short[].class, ArrayPreference.resolveArrayClass(new Short[]{2, null}));
		assertEquals(Integer[].class, ArrayPreference.resolveArrayClass(new Integer[]{1, null}));
		assertEquals(Float[].class, ArrayPreference.resolveArrayClass(new Float[]{1.5f, null}));
		assertEquals(Long[].class, ArrayPreference.resolveArrayClass(new Long[]{198L, null}));
		assertEquals(Double[].class, ArrayPreference.resolveArrayClass(new Double[]{15D, null}));
		assertEquals(String[].class, ArrayPreference.resolveArrayClass(new String[]{"", null}));
		assertEquals(null, ArrayPreference.resolveArrayClass(new Date[0]));
	}

	@Test public void testBooleanArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		boolean[] firstPrimitiveArray = (boolean[]) createTestableArrayOf(boolean.class);
		boolean[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Boolean.class);
	}

	@Test public void testByteArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		byte[] firstPrimitiveArray = (byte[]) createTestableArrayOf(byte.class);
		byte[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Byte.class);
	}

	@Test public void testCharArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		char[] firstPrimitiveArray = (char[]) createTestableArrayOf(char.class);
		char[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Character.class);
	}

	@Test public void testShortArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		short[] firstPrimitiveArray = (short[]) createTestableArrayOf(short.class);
		short[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Short.class);
	}

	@Test public void testIntegerArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		int[] firstPrimitiveArray = (int[]) createTestableArrayOf(int.class);
		int[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Integer.class);
	}

	@Test public void testFloatArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		float[] firstPrimitiveArray = (float[]) createTestableArrayOf(float.class);
		float[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Float.class);
	}

	@Test public void testLongArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		long[] firstPrimitiveArray = (long[]) createTestableArrayOf(long.class);
		long[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Long.class);
	}

	@Test public void testDoubleArray() {
		// Act + Assert:
		// Test storing and obtaining of primitive array.
		double[] firstPrimitiveArray = (double[]) createTestableArrayOf(double.class);
		double[] secondPrimitiveArray = putAndGetArray(firstPrimitiveArray);
		assertThat(secondPrimitiveArray, is(firstPrimitiveArray));
		// Test storing and obtaining of boxed array.
		innerTestPutAndGetArray(Double.class);
	}

	@Test public void testStringArray() {
		innerTestPutAndGetArray(String.class);
	}

	@Test
	public void testArrayWithNullItem() {
		// fixme: not working
		/*final String[] firstArray = new String[] {"text", null, null};
		final String[] secondArray = putGetArray(firstArray);
		assertThatArraysAreEqual(String[].class, firstArray, secondArray);*/
	}

	@SuppressWarnings("unchecked")
	private <T> void innerTestPutAndGetArray(Class<T> itemClass) {
		// Arrange:
		final T[] firstArray = (T[]) createTestableArrayOf(itemClass);
		// Act + Assert:
		assertThat(putAndGetArray(firstArray), is(firstArray));
	}

	private <A> A putAndGetArray(A array) {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		final ArrayPreference<A> preference = new ArrayPreference<>(PREF_KEY, null);
		preference.updateValue(array);
		// Act:
		preference.onPutIntoPreferences(preferences);
		preference.invalidate();
		return preference.onGetFromPreferences(preferences);
	}

	@Test public void testNotPersistedArray() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		// Act + Assert:
		assertThat(ArrayPreference.getFromPreferences(preferences, PREF_KEY, null), is(nullValue()));
	}

	@Test public void testNullArray() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		ArrayPreference.putIntoPreferences(preferences, PREF_KEY, null);
		// Act + Assert:
		assertThat(ArrayPreference.getFromPreferences(preferences, PREF_KEY, null), is(nullValue()));
	}

	@Test public void testUnsupportedArray() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		try {
			// Act:
			ArrayPreference.putIntoPreferences(preferences, PREF_KEY, new Date[]{new Date(0)});
			throw new AssertionError("No exception thrown.");
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Failed to put array of(Date) into shared preferences. " +
							"Only arrays of primitive types or theirs boxed representations including String are supported.")
			);
		}
		// Arrange:
		preferences.edit().putString(PREF_KEY, "<Date[]>[]").commit();
		try {
			// Act:
			ArrayPreference.getFromPreferences(preferences, PREF_KEY, null);
			throw new AssertionError();
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Failed to obtain an array of(Date) for the key(" + PREF_KEY + ") from shared preferences. " +
							"Only arrays of primitive types or theirs boxed representations including String are supported.")
			);
		}
	}

	@Test public void testArrayNotPersistedByLibrary1() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		preferences.edit().putString(PREF_KEY, "<String[]>text,text").commit();
		try {
			// Act:
			ArrayPreference.getFromPreferences(preferences, PREF_KEY, null);
		} catch (IllegalStateException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Trying to obtain an array for the key(" + PREF_KEY + ") from shared preferences not saved by the Preferences library.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testGetArrayNotPersistedByLibrary2() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:array", Context.MODE_PRIVATE);
		preferences.edit().putString(PREF_KEY, "[text,text,text]").commit();
		try {
			// Act:
			ArrayPreference.getFromPreferences(preferences, PREF_KEY, null);
		} catch (IllegalStateException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Trying to obtain an array for the key(" + PREF_KEY + ") from shared preferences not saved by the Preferences library.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testExtractArrayValueFromPreferenceValue() {
		// Act + Assert:
		assertThat(ArrayPreference.extractArrayElementsFromPreferenceValue(null), is(nullValue()));
		assertThat(ArrayPreference.extractArrayElementsFromPreferenceValue(""), is(nullValue()));
		assertThat(ArrayPreference.extractArrayElementsFromPreferenceValue("<int[]>[1, 2, 3]"), is("1, 2, 3"));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtAsNull() {
		// Arrange:
		final String[] array = (String[]) ArrayPreference.createArrayInSize(String.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(String[].class, array, i, Integer.toString(i));
			ArrayPreference.setArrayValueAt(String[].class, array, i, null);
			assertThat(array[i], is(nullValue()));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAt() {
		// Arrange:
		final String[] array = (String[]) ArrayPreference.createArrayInSize(String.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(String[].class, array, i, Integer.toString(i + 1));
			assertThat(array[i], is(Integer.toString(i + 1)));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtForByteArray() {
		// Arrange:
		final byte[] primitiveArray = (byte[]) ArrayPreference.createArrayInSize(byte.class, 10);
		for (int i = 0; i < primitiveArray.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(byte[].class, primitiveArray, i, i + 1);
			assertThat(primitiveArray[i], is((byte) (i + 1)));
		}
		// Arrange:
		final Byte[] array = (Byte[]) ArrayPreference.createArrayInSize(Byte.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(Byte[].class, array, i, i + 1);
			assertThat(array[i], is((byte) (i + 1)));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtForCharacterArray() {
		// Arrange:
		final char[] primitiveArray = (char[]) ArrayPreference.createArrayInSize(char.class, 10);
		for (int i = 0; i < primitiveArray.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(char[].class, primitiveArray, i, i + 1);
			assertThat(primitiveArray[i], is(Integer.toString(i + 1).charAt(0)));
		}
		// Arrange:
		final Character[] array = (Character[]) ArrayPreference.createArrayInSize(Character.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(Character[].class, array, i, i + 1);
			assertThat(array[i], is(Integer.toString(i + 1).charAt(0)));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtForShortArray() {
		// Arrange:
		final short[] primitiveArray = (short[]) ArrayPreference.createArrayInSize(short.class, 10);
		for (int i = 0; i < primitiveArray.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(short[].class, primitiveArray, i, i + 1);
			assertThat(primitiveArray[i], is((short) (i + 1)));
		}
		// Arrange:
		final Short[] array = (Short[]) ArrayPreference.createArrayInSize(Short.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(Short[].class, array, i, i + 1);
			assertThat(array[i], is((short) (i + 1)));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtForFloatArray() {
		// Arrange:
		final float[] primitiveArray = (float[]) ArrayPreference.createArrayInSize(float.class, 10);
		for (int i = 0; i < primitiveArray.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(float[].class, primitiveArray, i, i + 1);
			assertThat(primitiveArray[i], is((float) (i + 1)));
		}
		// Arrange:
		final Float[] array = (Float[]) ArrayPreference.createArrayInSize(Float.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(Float[].class, array, i, i + 1);
			assertThat(array[i], is((float) (i + 1)));
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testSetArrayValueAtForLongArray() {
		// Arrange:
		final long[] primitiveArray = (long[]) ArrayPreference.createArrayInSize(long.class, 10);
		for (int i = 0; i < primitiveArray.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(long[].class, primitiveArray, i, i + 1);
			assertThat(primitiveArray[i], is((long) (i + 1)));
		}
		// Arrange:
		final Long[] array = (Long[]) ArrayPreference.createArrayInSize(Long.class, 10);
		for (int i = 0; i < array.length; i++) {
			// Act + Assert:
			ArrayPreference.setArrayValueAt(Long[].class, array, i, i + 1);
			assertThat(array[i], is((long) (i + 1)));
		}
	}

	@Test public void testResolveArrayClassByName() {
		// Act + Assert:
		assertEquals(null, ArrayPreference.resolveArrayClassByName("Date[]"));
		// Primitive -------------------------------------------------------------------------------
		assertEquals(boolean[].class, ArrayPreference.resolveArrayClassByName("boolean[]"));
		assertEquals(byte[].class, ArrayPreference.resolveArrayClassByName("byte[]"));
		assertEquals(char[].class, ArrayPreference.resolveArrayClassByName("char[]"));
		assertEquals(short[].class, ArrayPreference.resolveArrayClassByName("short[]"));
		assertEquals(int[].class, ArrayPreference.resolveArrayClassByName("int[]"));
		assertEquals(float[].class, ArrayPreference.resolveArrayClassByName("float[]"));
		assertEquals(long[].class, ArrayPreference.resolveArrayClassByName("long[]"));
		assertEquals(double[].class, ArrayPreference.resolveArrayClassByName("double[]"));
		// Boxed -----------------------------------------------------------------------------------
		assertEquals(Boolean[].class, ArrayPreference.resolveArrayClassByName("Boolean[]"));
		assertEquals(Byte[].class, ArrayPreference.resolveArrayClassByName("Byte[]"));
		assertEquals(Character[].class, ArrayPreference.resolveArrayClassByName("Character[]"));
		assertEquals(Short[].class, ArrayPreference.resolveArrayClassByName("Short[]"));
		assertEquals(Integer[].class, ArrayPreference.resolveArrayClassByName("Integer[]"));
		assertEquals(Float[].class, ArrayPreference.resolveArrayClassByName("Float[]"));
		assertEquals(Long[].class, ArrayPreference.resolveArrayClassByName("Long[]"));
		assertEquals(Double[].class, ArrayPreference.resolveArrayClassByName("Double[]"));
		assertEquals(String[].class, ArrayPreference.resolveArrayClassByName("String[]"));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testCreateArrayInSize() {
		// Act + Assert:
		assertThat(ArrayPreference.createArrayInSize(Date.class, 1), is(nullValue()));
		// Primitive -------------------------------------------------------------------------------
		assertThat(((boolean[]) ArrayPreference.createArrayInSize(boolean.class, 1)).length, is(1));
		assertThat(((byte[]) ArrayPreference.createArrayInSize(byte.class, 2)).length, is(2));
		assertThat(((char[]) ArrayPreference.createArrayInSize(char.class, 3)).length, is(3));
		assertThat(((short[]) ArrayPreference.createArrayInSize(short.class, 4)).length, is(4));
		assertThat(((int[]) ArrayPreference.createArrayInSize(int.class, 5)).length, is(5));
		assertThat(((float[]) ArrayPreference.createArrayInSize(float.class, 6)).length, is(6));
		assertThat(((long[]) ArrayPreference.createArrayInSize(long.class, 7)).length, is(7));
		assertThat(((double[]) ArrayPreference.createArrayInSize(double.class, 8)).length, is(8));
		// Boxed -----------------------------------------------------------------------------------
		assertThat(((Boolean[]) ArrayPreference.createArrayInSize(Boolean.class, 1)).length, is(1));
		assertThat(((Byte[]) ArrayPreference.createArrayInSize(Byte.class, 2)).length, is(2));
		assertThat(((Character[]) ArrayPreference.createArrayInSize(Character.class, 3)).length, is(3));
		assertThat(((Short[]) ArrayPreference.createArrayInSize(Short.class, 4)).length, is(4));
		assertThat(((Integer[]) ArrayPreference.createArrayInSize(Integer.class, 5)).length, is(5));
		assertThat(((Float[]) ArrayPreference.createArrayInSize(Float.class, 6)).length, is(6));
		assertThat(((Long[]) ArrayPreference.createArrayInSize(Long.class, 7)).length, is(7));
		assertThat(((Double[]) ArrayPreference.createArrayInSize(Double.class, 8)).length, is(8));
	}

	private static <T> Object createTestableArrayOf(Class<T> itemClass) {
		if (boolean.class.equals(itemClass)) {
			return new boolean[]{true, false, true};
		} else if (byte.class.equals(itemClass)) {
			return new byte[]{48, 16};
		} else if (char.class.equals(itemClass)) {
			return new char[]{0, 1};
		} else if (short.class.equals(itemClass)) {
			return new short[]{3, 12};
		} else if (int.class.equals(itemClass)) {
			return new int[]{0, 10007, 12, 432342};
		} else if (float.class.equals(itemClass)) {
			return new float[]{15798.12f, 0.14578f};
		} else if (long.class.equals(itemClass)) {
			return new long[]{1654678987L, 11491465L};
		} else if (double.class.equals(itemClass)) {
			return new double[]{15498.161645578D, 12.1234971947D};
		} else if (Boolean.class.equals(itemClass)) {
			return new Boolean[]{true, false, true};
		} else if (Byte.class.equals(itemClass)) {
			return new Byte[]{48, 16};
		} else if (Character.class.equals(itemClass)) {
			return new Character[]{0, 1};
		} else if (Short.class.equals(itemClass)) {
			return new Short[]{3, 12};
		} else if (Integer.class.equals(itemClass)) {
			return new Integer[]{0, 10007, 12, 432342};
		} else if (Float.class.equals(itemClass)) {
			return new Float[]{15798.12f, 0.14578f};
		} else if (Long.class.equals(itemClass)) {
			return new Long[]{1654678987L, 11491465L};
		} else if (Double.class.equals(itemClass)) {
			return new Double[]{15498.161645578D, 12.1234971947D};
		} else if (String.class.equals(itemClass)) {
			return new String[]{"text", "text text", "text text"};
		} else if (Enum.class.equals(itemClass)) {
			return new Enum[]{TestEnum.WINTER, TestEnum.SUMMER};
		}
		return null;
	}
}