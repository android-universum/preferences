/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import universum.studios.android.test.AndroidTestCase;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CollectionPreferenceTest extends AndroidTestCase {

	private static final String PREF_KEY = "PREFERENCE.Collection";

	@Test public void testInstantiation() {
		// Act:
		final CollectionPreference<String> preference = new CollectionPreference<>(PREF_KEY, String.class, null);
		// Assert:
		assertThat(preference.getKey(), is(PREF_KEY));
		assertThat(preference.getDefaultValue(), is(nullValue()));
	}

	@Test public void testBooleanCollection() {
		testPutAndGetCollectionTypeOf(Boolean.class);
	}

	@Test public void testByteCollection() {
		testPutAndGetCollectionTypeOf(Byte.class);
	}

	@Test public void testShortCollection() {
		testPutAndGetCollectionTypeOf(Short.class);
	}

	@Test public void testIntegerCollection() {
		testPutAndGetCollectionTypeOf(Integer.class);
	}

	@Test public void testFloatCollection() {
		testPutAndGetCollectionTypeOf(Float.class);
	}

	@Test public void testLongCollection() {
		testPutAndGetCollectionTypeOf(Long.class);
	}

	@Test public void testDoubleCollection() {
		testPutAndGetCollectionTypeOf(Double.class);
	}

	@Test public void testEmptyCollection() {
		innerTestPutAndGetCollection(new ArrayList<>(0), Integer.class);
	}

	@Test public void testNotPersistedCollection() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:collection", Context.MODE_PRIVATE);
		// Act + Assert:
		assertThat(CollectionPreference.getFromPreferences(preferences, PREF_KEY, null), is(nullValue()));
	}

	@Test public void testNullCollection() {
		innerTestPutAndGetCollection(null, null);
	}

	private <T> void testPutAndGetCollectionTypeOf(Class<T> componentType) {
		innerTestPutAndGetCollection(createTestCollectionOf(componentType), componentType);
	}

	private <T> void innerTestPutAndGetCollection(Collection<T> collection, Class<T> componentType) {
		final SharedPreferences preferences = context().getSharedPreferences("preferences:collection", Context.MODE_PRIVATE);
		final CollectionPreference<T> preference = new CollectionPreference<>(PREF_KEY, componentType, null);
		preference.updateValue(collection);
		assertThat(preference.onPutIntoPreferences(preferences), is(any(Boolean.class)));
		preference.invalidate();
		assertThat(preference.onGetFromPreferences(preferences), is(collection));
	}

	@Test public void testUnsupportedCollection() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:collection", Context.MODE_PRIVATE);
		final Collection<Date> collection = new ArrayList<>(1);
		collection.add(new Date(0));
		try {
			// Act:
			assertTrue(CollectionPreference.putIntoPreferences(preferences, PREF_KEY, collection, Date.class));
			throw new AssertionError("No exception thrown.");
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Failed to put collection of(Date) into shared preferences. " +
							"Only collections of primitive types or theirs boxed representations including String are supported.")
			);
		}
		// Arrange:
		preferences.edit().putString(PREF_KEY, "<Date[]>[]").commit();
		try {
			// Act:
			CollectionPreference.getFromPreferences(preferences, PREF_KEY, null);
			throw new AssertionError("No exception thrown.");
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Failed to obtain collection for the key(" + PREF_KEY + ") from shared preferences. " +
							"Only collections of primitive types or theirs boxed representations including String are supported.")
			);
		}
	}

	@Test public void testCollectionNotPersistedByLibrary1() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:collection", Context.MODE_PRIVATE);
		preferences.edit().putString(PREF_KEY, "<String[]>text,text").commit();
		try {
			// Act:
			CollectionPreference.getFromPreferences(preferences, PREF_KEY, null);
			throw new AssertionError("No exception thrown.");
		} catch (IllegalStateException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Trying to obtain a collection for the key(" + PREF_KEY + ") from shared preferences not saved by the Preferences library.")
			);
		}
	}

	@Test public void testCollectionNotPersistedByLibrary2() {
		// Arrange:
		final SharedPreferences preferences = context().getSharedPreferences("preferences:collection", Context.MODE_PRIVATE);
		preferences.edit().putString(PREF_KEY, "[text, text, text]").commit();
		try {
			// Act:
			CollectionPreference.getFromPreferences(preferences, PREF_KEY, null);
		} catch (IllegalStateException e) {
			// Assert:
			assertThat(
					e.getMessage(),
					is("Trying to obtain a collection for the key(" + PREF_KEY + ") from shared preferences not saved by the Preferences library.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@SuppressWarnings("unchecked")
	private static <T> Collection<T> createTestCollectionOf(Class<T> itemClass) {
		final Collection collection = new ArrayList<>();
		if (Boolean.class.equals(itemClass)) {
			collection.add(true);
			collection.add(true);
			collection.add(false);
		} else if (Byte.class.equals(itemClass)) {
			collection.add((byte) 10);
			collection.add((byte) 15);
			collection.add((byte) 124);
			collection.add((byte) 2);
		} else if (Short.class.equals(itemClass)) {
			collection.add((short) 26);
			collection.add((short) 64);
		} else if (Integer.class.equals(itemClass)) {
			collection.add(45);
			collection.add(9784);
			collection.add(15498798);
			collection.add(1);
			collection.add(13241657);
		} else if (Float.class.equals(itemClass)) {
			collection.add(15.2f);
			collection.add(1657.154f);
			collection.add(13487.155614f);
		} else if (Long.class.equals(itemClass)) {
			collection.add(1645L);
			collection.add(1L);
			collection.add(6749841687984L);
		} else if (Double.class.equals(itemClass)) {
			collection.add(15648979465.1634616D);
			collection.add(654777.22D);
		} else if (String.class.equals(itemClass)) {
			collection.add("text text");
			collection.add("text text text text");
			collection.add("text text");
			collection.add("text text text");
		}
		return (Collection<T>) collection;
	}
}